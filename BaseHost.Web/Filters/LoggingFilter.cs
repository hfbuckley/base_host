using System.Diagnostics;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog;
using ILogger = Microsoft.Extensions.Logging.ILogger;

// ReSharper disable ClassNeverInstantiated.Global

namespace BaseHost.Web.Filters;

public class LoggingFilter : IActionFilter
{
    private ILogger _logger = null!;
    private readonly Stopwatch _stopwatch = new();
    public void OnActionExecuting(ActionExecutingContext context)
    {
        _stopwatch.Start();
        var loggerType = typeof(ILogger<>).MakeGenericType(context.Controller.GetType());
        _logger = (ILogger) context.HttpContext.RequestServices
            .GetService(loggerType)!;
        var url =
            $"{context.HttpContext.Request.Scheme}://{context.HttpContext.Request.Host}" +
            $"{context.HttpContext.Request.Path}{context.HttpContext.Request.QueryString}";
        
        _logger
            .LogInformation("Request {Method} {Url} Received ({Controller}.{ActionMethod}) from {IP} {UserAgent}",
                context.HttpContext.Request.Method,
                url,
                context.Controller.GetType().Name,
                (context.ActionDescriptor as ControllerActionDescriptor)?.ActionName 
                ?? context.ActionDescriptor.DisplayName,
                context.HttpContext.Connection.RemoteIpAddress?.ToString(),
                context.HttpContext.Request.Headers.UserAgent.ToString().Split(" ").Last());
    }
        
    public void OnActionExecuted(ActionExecutedContext context)
    {
        _stopwatch.Stop();
        var env = context.HttpContext.RequestServices.GetService<IHostEnvironment>()!;
        var url =
            $"{context.HttpContext.Request.Scheme}://{context.HttpContext.Request.Host}" +
            $"{context.HttpContext.Request.Path}{context.HttpContext.Request.QueryString}";
        
        context.HttpContext.Response.Headers["Time-Taken-To-Process-Ms"] = _stopwatch.ElapsedMilliseconds.ToString();
        context.HttpContext.Response.Headers["Server-Environment"] = env.EnvironmentName;
        context.HttpContext.Response.Headers["Server-Name"] = Assembly.GetEntryAssembly()?.GetName().Name;
        context.HttpContext.Response.Headers["Server-Version"] =
            Assembly.GetEntryAssembly()?.GetName().Version?.ToString();
        
        _logger
            .LogInformation("Request {Method} {Url} ({Controller}.{Action}) finished after {Elapsed}Ms {IP} {UserAgent}",
                context.HttpContext.Request.Method,
                url,
                context.Controller.GetType().Name,
                (context.ActionDescriptor as ControllerActionDescriptor)?.ActionName 
                ?? context.ActionDescriptor.DisplayName,
                _stopwatch.ElapsedMilliseconds,
                context.HttpContext.Connection.RemoteIpAddress?.ToString(),
                context.HttpContext.Request.Headers.UserAgent.ToString().Split(" ").Last());
    }

    public Task HandleUnhandledMethod(HttpContext i)
    {
        var logger = LogManager.GetCurrentClassLogger();
        var env = i.RequestServices.GetService<IHostEnvironment>()!;

        var url =
            $"{i.Request.HttpContext.Request.Scheme}://{i.Request.HttpContext.Request.Host}" +
            $"{i.Request.HttpContext.Request.Path}{i.Request.HttpContext.Request.QueryString}";

        logger
            .Warn("Unmapped Request {Method} {Url} from {IP} {UserAgent}",
                i.Request.HttpContext.Request.Method,
                url,
                i.Request.HttpContext.Connection.RemoteIpAddress?.ToString(),
                i.Request.HttpContext.Request.Headers.UserAgent);

        i.Response.Headers["Server-Environment"] = env.EnvironmentName;
        i.Response.Headers["Server-Name"] = Assembly.GetEntryAssembly()?.GetName().Name;
        i.Response.Headers["Server-Version"] =
            Assembly.GetEntryAssembly()?.GetName().Version?.ToString();
        i.Response.StatusCode = 404;
        return Task.CompletedTask;
    }
}