using System.ComponentModel;
using System.Reflection;
using BaseHost.Generators;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

// ReSharper disable ClassNeverInstantiated.Global

namespace BaseHost.Web.Filters;

public class DescriptionSchemaFilter :
    ISchemaFilter, IDocumentFilter, IParameterFilter, IOperationFilter, IRequestBodyFilter
{
    public void Apply(OpenApiSchema schema, SchemaFilterContext context)
    {
        schema.Description ??= context.ParameterInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        schema.Description ??= context.MemberInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        schema.Description ??= context.Type?.GetCustomAttribute<DescriptionAttribute>()?.Description;
                
        schema.Title ??= context.ParameterInfo?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        schema.Title ??= context.MemberInfo?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        schema.Title ??= context.Type?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        schema.Title ??= context.Type?.Name;
                
        var examples = GetExamples((context.ParameterInfo?.GetCustomAttributes<ExampleAttribute>()
                                    ?? context.MemberInfo?.GetCustomAttributes<ExampleAttribute>()
                                    ?? context.Type?.GetCustomAttributes<ExampleAttribute>()) ?? []);
        
        schema.Example ??= examples.FirstOrDefault().Value?.Value;
    }
        
    public void Apply(OpenApiParameter parameter, ParameterFilterContext context)
    {
        parameter.Description ??= context.ParameterInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        parameter.Description ??= context.PropertyInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
                
        parameter.Name ??= context.ParameterInfo?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        parameter.Name ??= context.PropertyInfo?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        parameter.Name ??= context.PropertyInfo?.Name;

        var examples = GetExamples((context.ParameterInfo?.GetCustomAttributes<ExampleAttribute>()
                                    ?? context.PropertyInfo?.GetCustomAttributes<ExampleAttribute>()) ?? []);
                
        parameter.Example ??= examples.FirstOrDefault().Value?.Value;
        parameter.Examples = examples;
    }
        
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        operation.Description ??= context.MethodInfo?.GetCustomAttribute<DescriptionAttribute>()?.Description;
        operation.OperationId ??= context.MethodInfo?.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
        operation.OperationId ??= context.MethodInfo?.Name;
        
        if (operation.Tags?.Count != 1) return;
        operation.Tags[0].Name = context.MethodInfo?.DeclaringType?
            .GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? operation.Tags[0].Name;
    }
        
    public void Apply(OpenApiRequestBody requestBody, RequestBodyFilterContext context)
    {
    }
    
    public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
    {
        var controllers = context.ApiDescriptions.Select(i => i.ActionDescriptor)
            .OfType<ControllerActionDescriptor>()
            .Select(i => i.ControllerTypeInfo).Distinct().ToList();
        
        foreach (var controller in controllers)
        {
            var existingTag = swaggerDoc.Tags.FirstOrDefault(i => i.Name == controller.Name);
            if(existingTag!=null) swaggerDoc.Tags.Remove(existingTag);
            swaggerDoc.Tags.Add(new OpenApiTag
            {
                Name = controller.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName ?? controller.Name.Replace("Controller",string.Empty),
                Description = controller.GetCustomAttribute<DescriptionAttribute>()?.Description ?? null
            });
        }
    }
            
    private static IOpenApiAny? GetExample(ExampleAttribute? example)
    {
        if (example == null) return null;

        return example.Type switch
        {
            ExampleAttribute.Types.Int => new OpenApiInteger(int.Parse(example.Value!)),
            ExampleAttribute.Types.Double => new OpenApiDouble(double.Parse(example.Value!)),
            ExampleAttribute.Types.Byte => new OpenApiByte(byte.Parse(example.Value!)),
            ExampleAttribute.Types.Bool => new OpenApiBoolean(bool.Parse(example.Value!)),
            ExampleAttribute.Types.DateTime => new OpenApiDateTime(DateTime.Parse(example.Value!)),
            ExampleAttribute.Types.Date => new OpenApiDate(DateTime.Parse(example.Value!).Date),
            ExampleAttribute.Types.Object => new OpenApiObject(),
            ExampleAttribute.Types.String => new OpenApiString(example.Value),
            _ => new OpenApiString(example.Value)
        };
        
       // return new OpenApiString(example.Value);
        // return example.Value != null ? new OpenApiString(example.Value)
        //     : example?.Int != null ? new OpenApiInteger(example.Int.Value)
        //     : example?.Double != null ? new OpenApiDouble(example.Double.Value)
        //     : example?.Byte != null ? new OpenApiByte(example.Byte.Value)
        //     : example?.Bool != null ? new OpenApiBoolean(example.Bool.Value) : null;
    }
            
    private static Dictionary<string,OpenApiExample> GetExamples(IEnumerable<ExampleAttribute> attributes)
    {
        return attributes
            .Select((i, c) => (i, c)).ToDictionary(i => i.i.Name ?? i.i.Description ?? i.c.ToString(),
                i => new OpenApiExample
                {
                    Value = GetExample(i.i),
                    Summary = i.i.Name ?? i.i.Description ?? i.c.ToString(),
                    Description = i.i.Description ?? i.i.Name
                });
    }
}