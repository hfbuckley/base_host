using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using BaseHost.Contract;
using BaseHost.Extensions;
using BaseHost.Host;
using BaseHost.Modules.Config;
using BaseHost.Modules.Logging;
using BaseHost.Web.Filters;
using BaseHost.Web.GraphQL;
using CommandLine;
using HotChocolate.Execution.Configuration;
using HotChocolate.Types.Descriptors;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Converters;
using NLog;
using NLog.Web;

// ReSharper disable VirtualMemberNeverOverridden.Global
// ReSharper disable UnusedMember.Global

namespace BaseHost.Web;

public abstract class AspNetHost
{
    protected static void Startup<T>(string[] args) where T : AspNetHost, new ()
        => Parser.Default.ParseArguments<Options>(args)
            .WithParsed(s =>
            {
                try
                {
                    var name = s.ApplicationName ?? typeof(T).Assembly.GetName().Name;
                    NLogConfig.Setup($"{s.LogLocation}{name}.log", s.ConsoleLogLevel ?? "info", s.LogLevel ?? "debug");

                    var env = s.Environment
                              ?? Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT")
                              ?? Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")
                              ?? "Production";

                    var logger = LogManager.GetCurrentClassLogger();
                    logger.Info($"Starting {name} {Assembly.GetEntryAssembly()?.GetName().Version}");
                    logger.Info($"Exe: {Environment.GetCommandLineArgs().FirstOrDefault()}");
                    logger.Info($"Args: {Environment.GetCommandLineArgs().Skip(1).JoinString(" ")}");
                    logger.Info($"Env: {env}");
                    
                    var host = new T()
                        .ConfigureHostBuilder<T>(Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder())
                        .Build();
                    
                    host.Start();

                    var server = host.Services.GetService<IServer>()!;
                    var addressFeature = server.Features.Get<IServerAddressesFeature>()!;
                    foreach (var webAddress in addressFeature.Addresses)
                        logger.Info("WebAddress: {WebAddress}", webAddress);

                    host.WaitForShutdown();
                }
                catch (Exception ex)
                {
                    LogManager
                        .GetLogger("ExceptionLogger")
                        .Error(ex, "An exception has been thrown, will now close.");
                }
            });

    protected virtual IHostBuilder ConfigureHostBuilder<TStartUp>(IHostBuilder builder) where TStartUp : class
    {
        return builder.UseServiceProviderFactory(new AutofacServiceProviderFactory(LoadContainer))
            .ConfigureWebHostDefaults(wb => wb.UseStartup<TStartUp>())
            .ConfigureLogging(logging => logging.ClearProviders())
            .UseNLog();
    }
    
    protected virtual void LoadContainer(ContainerBuilder builder){
        builder.RegisterModule<NLogModule>();
        builder.RegisterModule<ConfigModule>();
        builder
            .RegisterType<ConsoleSchedulerProvider>()
            .As<ISchedulerProvider>()
            .SingleInstance();
    
        SetupContainer(builder);
    }

    protected virtual void SetupSwaggerServer(IApplicationBuilder app)
        => app
            .UseSwagger()
            .UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/openapi.json", "v1");
                options.ShowCommonExtensions();
                options.ShowExtensions();
                options.EnableFilter();
                options.EnableValidator();
                options.EnableDeepLinking();
                options.RoutePrefix = string.Empty;
            });
    
    protected virtual void SetupSwaggerGen(IServiceCollection services)
        => services
            .AddOpenApi()
            .AddEndpointsApiExplorer()
            .AddSwaggerGen(c =>
            {
                c.UseOneOfForPolymorphism();
                c.UseAllOfForInheritance();
                c.UseAllOfToExtendReferenceSchemas();
                c.SchemaFilter<DescriptionSchemaFilter>();
                c.SchemaFilter<RequireNonNullablePropertiesSchemaFilter>();
                c.DocumentFilter<DescriptionSchemaFilter>();
                c.ParameterFilter<DescriptionSchemaFilter>();
                c.OperationFilter<DescriptionSchemaFilter>();
                c.RequestBodyFilter<DescriptionSchemaFilter>();
                c.SupportNonNullableReferenceTypes();
                c.SelectSubTypesUsing(baseType =>
                    baseType.IsInterface 
                        ? baseType.Assembly.GetTypes()
                            .Where(baseType.IsAssignableFrom).Where(p => !p.IsInterface) : []);
            })
            .AddSwaggerGenNewtonsoftSupport();

    public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        SetupSwaggerServer(app);
        app
            .UseRouting()
            .UseEndpoints(e =>
            {
                e.MapGraphQL();
                e.MapOpenApi("openapi.json");
                e.MapGraphQLVoyager("voyager");
                e.MapGraphQLGraphiQL("graphiql");
                e.MapControllers();
                e.MapFallback(i => HandleFallback(i,env));
            });
        SetupApp(app, env);
    }

    public virtual void ConfigureServices(IServiceCollection services){
        services
            .AddControllers(i =>
            {
                i.OutputFormatters.RemoveType<HttpNoContentOutputFormatter>();
                i.OutputFormatters.RemoveType<StringOutputFormatter>();
                i.OutputFormatters.RemoveType<StreamOutputFormatter>();
                i.Filters.Add<LoggingFilter>();
            })
            .AddNewtonsoftJson(c =>
            {
                c.SerializerSettings.Converters.Add(new StringEnumConverter());
                c.SerializerSettings.ContractResolver = new EnumDefaultValueContractResolver();
            });

        services.AddRouting().AddCors();
        
        SetupSwaggerGen(services);
        
        InnerSetupGraphQl(services);
            
        SetupServices(services);
    }
    
    protected virtual void InnerSetupGraphQl(IServiceCollection services)
    {
        var build = services.AddGraphQLServer()
            .AddConvention<INamingConventions, DescriptionNamingConventions>();

        var controllers = (Assembly.GetEntryAssembly()?.GetTypes()
            .Where(i => i.IsAssignableTo<Controller>())
            .Where(i => i.GetCustomAttribute<ApiControllerAttribute>() != null) ?? [])
            .ToList();

        if (controllers.Any(ControllerStatic.AnyQueryMethod))
            build.AddQueryType<OperationTypes.Query>();
        if (controllers.Any(ControllerStatic.AnyMutationMethod))
            build.AddMutationType<OperationTypes.Mutation>();
        if (controllers.Any(ControllerStatic.AnySubscriptionMethod))
            build.AddSubscriptionType<OperationTypes.Subscription>();
        
        controllers.ForEach(c =>
        {
            build.AddTypeExtension(typeof(ControllerQueryType<>).MakeGenericType(c));
            build.AddTypeExtension(typeof(ControllerMutationType<>).MakeGenericType(c));
            build.AddTypeExtension(typeof(ControllerSubscriptionType<>).MakeGenericType(c));
        });
        SetupGraphQL(build);

        build.ModifyOptions(o =>
            {
                o.EnableOneOf = true;
                o.StrictValidation = true;
                o.StrictRuntimeTypeValidation = true;
                o.RemoveUnreachableTypes = true;
                o.RemoveUnusedTypeSystemDirectives = true;
            })
            .TrimTypes();
    }

    private Task HandleFallback(HttpContext i, IWebHostEnvironment env)
    {
        var logger = LogManager.GetCurrentClassLogger();
        var url =
            $"{i.Request.HttpContext.Request.Scheme}://{i.Request.HttpContext.Request.Host}" +
            $"{i.Request.HttpContext.Request.Path}{i.Request.HttpContext.Request.QueryString}";

        logger
            .Warn("Unmapped Request {Method} {Url} from {IP} {UserAgent}",
                i.Request.HttpContext.Request.Method,
                url,
                i.Request.HttpContext.Connection.RemoteIpAddress?.ToString(),
                i.Request.HttpContext.Request.Headers.UserAgent);

        i.Response.Headers["Server-Environment"] = env.EnvironmentName;
        i.Response.Headers["Server-Name"] = Assembly.GetEntryAssembly()?.GetName().Name;
        i.Response.Headers["Server-Version"] =
            Assembly.GetEntryAssembly()?.GetName().Version?.ToString();
        i.Response.StatusCode = 404;
        return Task.CompletedTask;
    }

    protected virtual void SetupContainer(ContainerBuilder builder) {}
    protected virtual void SetupServices(IServiceCollection services){}
    protected virtual void SetupGraphQL(IRequestExecutorBuilder build){}
    protected virtual void SetupApp(IApplicationBuilder app, IWebHostEnvironment env) {}
}