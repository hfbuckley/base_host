// ReSharper disable ClassNeverInstantiated.Global

namespace BaseHost.Web.GraphQL;

public class OperationTypes
{
    [GraphQLDescription("Top level for all Queries")]
    public class Query;
    [GraphQLDescription("Top level for all Mutations")]
    public class Mutation;
    [GraphQLDescription("Top level for all Subscriptions")]
    public class Subscription;
}