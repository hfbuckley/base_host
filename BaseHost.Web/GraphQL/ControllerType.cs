using System.Reflection;
using BaseHost.Extensions;
using Microsoft.AspNetCore.Mvc;

// ReSharper disable UnusedType.Global
// ReSharper disable ClassNeverInstantiated.Global

namespace BaseHost.Web.GraphQL;

public static class ControllerStatic {
    internal static readonly HashSet<string> ParentMembers = 
        typeof(Controller).GetMembers().Select(i => i.Name).ToHashSet();
    
    public static bool FilterQueryMember(MemberInfo i) 
        => i.GetCustomAttribute<HttpGetAttribute>() != null;
    public static bool FilterMutationMember(MemberInfo i) 
        => i.GetCustomAttribute<HttpPutAttribute>() != null
           || i.GetCustomAttribute<HttpPostAttribute>() != null
           || i.GetCustomAttribute<HttpDeleteAttribute>() != null;
    public static bool FilterSubscriptionMember(MemberInfo i) 
        => (i as MethodInfo)?.GetCustomAttribute<RouteAttribute>() != null
           && (i as MethodInfo)?.ReturnType.IsAssignableTo(typeof(IObservable<>)) == true;

    public static IEnumerable<MemberInfo> GetMethods(Type type) => type.GetMembers()
        .Where(i => !ParentMembers.Contains(i.Name))
        .Where(i => i.MemberType.HasFlag(MemberTypes.Method) || i.MemberType.HasFlag(MemberTypes.Property))
        .Where(i => (i as MethodInfo)?.ReturnType != typeof(HttpResponseMessage)
                    && (i as PropertyInfo)?.PropertyType != typeof(HttpResponseMessage));

    public static bool AnyQueryMethod(Type type) => GetMethods(type).Where(FilterQueryMember).Any();
    public static bool AnyMutationMethod(Type type) => GetMethods(type).Where(FilterMutationMember).Any();
    public static bool AnySubscriptionMethod(Type type) => GetMethods(type).Where(FilterSubscriptionMember).Any();
}

public abstract class ControllerType<T,TExtends> : ObjectTypeExtension<T> where T : Controller
{
    protected override void Configure(IObjectTypeDescriptor<T> descriptor)
    {
        descriptor.BindFieldsExplicitly();
        descriptor.ExtendsType<TExtends>();
        
        FilterMembers(ControllerStatic.GetMethods(typeof(T))).ForEach(m => descriptor.Field(m));
        base.Configure(descriptor);
    }
    protected abstract IEnumerable<MemberInfo> FilterMembers(IEnumerable<MemberInfo> self);
}

public class ControllerQueryType<T> :  ControllerType<T,OperationTypes.Query> where T : Controller
{
    protected override IEnumerable<MemberInfo> FilterMembers(IEnumerable<MemberInfo> self) 
        => self.Where(ControllerStatic.FilterQueryMember);
}

public class ControllerMutationType<T> :  ControllerType<T,OperationTypes.Mutation> where T : Controller
{
    protected override IEnumerable<MemberInfo> FilterMembers(IEnumerable<MemberInfo> self) 
        => self.Where(ControllerStatic.FilterMutationMember);
}

public class ControllerSubscriptionType<T> :  ControllerType<T,OperationTypes.Subscription> where T : Controller
{
    protected override IEnumerable<MemberInfo> FilterMembers(IEnumerable<MemberInfo> self) 
        => self.Where(ControllerStatic.FilterSubscriptionMember);
}