using System.ComponentModel;
using System.Reflection;
using HotChocolate.Types.Descriptors;

// ReSharper disable ClassNeverInstantiated.Global

namespace BaseHost.Web.GraphQL;

public class DescriptionNamingConventions : DefaultNamingConventions
{
    public override string? GetMemberDescription(
        MemberInfo member,
        MemberKind kind)
        => base.GetMemberDescription(member, kind) ?? member
            .GetCustomAttributes(typeof(DescriptionAttribute), true)
            .OfType<DescriptionAttribute>().FirstOrDefault()?.Description;

    public override string? GetTypeDescription(Type type, TypeKind kind)
        => base.GetTypeDescription(type, kind) ?? type
            .GetCustomAttributes(typeof(DescriptionAttribute), true)
            .OfType<DescriptionAttribute>().FirstOrDefault()?.Description;

    public override string? GetEnumValueDescription(object value)
    {
        var enumType = value.GetType();
        if (enumType.IsEnum)
        {
            return base.GetEnumValueDescription(value) ?? enumType
                .GetMember(value.ToString()!)
                .FirstOrDefault()?
                .GetCustomAttributes(typeof(DescriptionAttribute), true)
                .OfType<DescriptionAttribute>().FirstOrDefault()?.Description;
        }
        return null;
    }

    public override string? GetArgumentDescription(ParameterInfo parameter)
        => base.GetArgumentDescription(parameter) ?? parameter
            .GetCustomAttributes(typeof(DescriptionAttribute), true)
            .OfType<DescriptionAttribute>().FirstOrDefault()?.Description;

}