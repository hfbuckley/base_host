using System.ComponentModel;
using JetBrains.Annotations;
using BaseHost.Generators;

namespace AspHostRunner;

[BuilderPattern]
[DisplayName("My Model")]
[Description("Represents things")]
[PublicAPI]
public partial record Model
{
    [Example(Name = "N",Description = "Normal",Value = "1001351")]
    [Example(Name = "E",Description = "Extraordinary",Value = "*&%££%&$@^*")]
    [Description("uniquely id a model")]
    public string Id { get; private set; }

    public string? ThisIsAThing { get; set; } = "THE";
}