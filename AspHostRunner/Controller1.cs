using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BaseHost.Generators;
using BaseHost.Modules.Config;
using Microsoft.AspNetCore.Mvc;

namespace AspHostRunner;

[ApiController,Route("/test")]
[DisplayName("My Controller")]
[Description("Lots of interesting functions")]
public class Controller1(IConfiguration configuration,Controller1.Test1 config) : Controller
{
    [Config("Test1")]
    public record Test1(string The);

    [HttpGet("GetEnv2")]
    [Description("It gets the 'The' config")]
    [DisplayName("Env")]
    public string The() => config.The;
    
    [HttpGet("GetEnv")]
    [Description("It gets the env")]
    [DisplayName("Env")]
    public string GetEnv() => configuration["Env"]!;
    
    [HttpGet("GetMyName")]
    [Description("It gets my name")]
    [DisplayName("MyName")]
    public Model GetMyName() => Model.CreateBuilder().WithId("te").Build();
    
    [HttpGet("SetMyName")]
    [Description("It sets my name")]
    [DisplayName("MyNameSetter")]
    public Model GetMyName(
        [Example(Name = "N", Description = "Normal",Value = "1001351")]
        [Example(Name = "E", Description = "Extraordinary",Value = "*&%££%&$@^*")]
        [Description("new id")]
        [Required] 
        string id) => Model.CreateBuilder().WithId(id).Build();
    
    [HttpPut("SetMyNamePut")]
    [Description("It sets my name")]
    [DisplayName("MyNameSetterPutter")]
    public Model GetMyNamePut(
        [Example(Name = "N", Description = "Normal",Value = "1001351")]
        [Example(Name = "E", Description = "Extraordinary",Value = "*&%££%&$@^*")]
        [Description("new id")]
        [Required] 
        string id,
        [Example(Name = "N", Description = "Normal",Value = "12", Type = ExampleAttribute.Types.Int)]
        [Example(Name = "E", Description = "Extraordinary",Value = "1001351",Type = ExampleAttribute.Types.Int)]
        [Description("new number")]
        [Required]
        int number) => Model.CreateBuilder().WithId(id).WithThisIsAThing(number.ToString()).Build();

    
    
    [HttpGet("HttpResponseMessage")]
    public int ReturnHttpMessage() => 0;
}