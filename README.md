## Purpose

This is used as the foundation of my other C# projects. It provides some common bits of setup with are often needed. It sets up a configured host which uses my config module, my console module, Autofac and NLog. There is also a code generator which will create builders for any class [BuilderPattern]. 

## How to use

### Host

This is how a host can be setup, you'll need to add your own nlog config

```c#
public class Host : ConsoleHost{
    public static void Main(string[] args) => new Host().Start(args);
    protected override void Setup(ContainerBuilder builder){
        builder.AutoActivateWith<Controller>(i => i.Start());
    }
    public class Controller{
        public Controller(ILogger logger){...}
        public void Start(){...}
    }
}
```

### Builder Generator

The generator will create a builder class, also add methods for each public property with a private mutator, With(value), Update(Func<value,value>). It will also make special use of ImmutableArray, ImmutableDictionary and ImmutableHashSet

Example model class

```c#
[BuilderPattern]
public partial class Model
{
    public string Value { get; private set; }
}
```

generated class

```c#
public partial class Model
{
    private Model () { }
    public Builder AsBuilder() => Builder.BasedOn(this);
    public static Builder CreateBuilder() => Builder.Instance();

    public Mutable AsMutable() => new Mutable(){
        Value = this.Value,
    };
    
public Model WithValue(string value) => AsBuilder().WithValue(value).Build();
public Model UpdateValue(Func<string,string> valueFunc) => AsBuilder().UpdateValue(valueFunc).Build();
    public sealed partial class Builder {
        private Model _instance = new Model();
        private Builder() { }
        internal static Builder Instance () => new Builder();
        internal static Builder BasedOn(Model reference) => Instance ()
        .WithValue(reference.Value);                    
        
        
    
    public Builder WithValue(string value){
        _instance.Value = value;
        return this;
    }
    public Builder UpdateValue(Func<string,string> valueFunc){
        _instance.Value = valueFunc(_instance.Value);
        return this;
    }
        public Model Build() {
            var local = _instance;
            _instance = null;
            local = local.GetValidatedInstance();
            
            return local;
        }
    }

    public sealed class Mutable {
        public string Value {get; set; }

        public Model AsImmutable() => Model.CreateBuilder()
            .WithValue(this.Value)
            .Build();                   

}}}
```

### Console

There are shells, console and commands. shells run on a console, and have access to commands. new commands can be registered and them will be accessible from all the consoles. There are some commands included, for shutdown/get-starting-args/config and more commands can be registered in ICommandManager 


There is also a websocket interface for this.

```c#
var shutdown = new Shutdown();

_commandManager.Register("get-starting-args",args);
```

### Config

A config can be given in the form of a json file (Host.config.json), if no json file is provided one will be generated and saved in output at runtime.
Config is specified as below:
```c#
    [ConfigValue(Name = "ws_pokemon_server_path", DefaultString = "ws:bhah.blah.blah:12")]
    private static string ConnectionString => Config.ws_pokemon_server_path; //Config is generated at build time
```

### Extension Methods
Lots of extension methods, covering a variety of functionality, enumerable/observables/functions ..., not really much structure, just whatever I've found useful 


