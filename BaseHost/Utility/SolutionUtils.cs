namespace BaseHost.Utility;

public static class SolutionUtils   
{
    public static DirectoryInfo? TryGetSolutionDirectoryInfo(string? currentPath = null)
    {
        var directory = new DirectoryInfo(currentPath ?? Directory.GetCurrentDirectory());
        while (directory != null && (!directory.GetFiles("*.sln").Any() || !directory.GetFiles("*.slnx").Any()))
        {
            directory = directory.Parent;
        }
        return directory;
    }
}