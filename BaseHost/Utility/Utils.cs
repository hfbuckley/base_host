namespace BaseHost.Utility;

// ReSharper disable once ClassNeverInstantiated.Global
public class Utils
{
    [ThreadStatic]
    private static Random? _random;

    public static Random Random => _random 
        ??= new Random((int) ((1+Thread.CurrentThread.ManagedThreadId) * DateTime.UtcNow.Ticks) );
}