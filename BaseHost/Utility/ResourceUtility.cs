using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BaseHost.Utility;

public static class ResourceUtility
{
    private static readonly JsonSerializerSettings Settings = new()
    {
        TypeNameHandling = TypeNameHandling.None,
        DefaultValueHandling = DefaultValueHandling.Ignore,
        Formatting = Formatting.Indented,
        Converters = new List<JsonConverter>
        {
            new StringEnumConverter()
        }
    };
        
    public static Dictionary<TId,TItem> DeserializeFromStream<TId,TItem>(
        StreamReader stream, 
        Func<TItem,TId> idSelector,
        JsonSerializerSettings? serializerSettings= null)
    {
        try
        {
            return (JsonSerializer.Create(serializerSettings??Settings).Deserialize(stream, typeof(List<TItem>))
                    as List<TItem> ?? throw new Exception("Can't deserialize object"))
                .ToDictionary(idSelector);
        }
        catch
        {
            return new Dictionary<TId, TItem>();
        }
    }
}