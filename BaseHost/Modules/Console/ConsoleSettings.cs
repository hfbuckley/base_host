using BaseHost.Modules.Config;

namespace BaseHost.Modules.Console;

[Config("Console")]
public record ConsoleSettings(
    bool EnableLocalConsole = false,
    string ShellPrompt = ">:",
    string WelcomeMessage = "");