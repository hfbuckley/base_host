﻿using BaseHost.Modules.Console.Console;
using BaseHost.Modules.Console.Manager;

namespace BaseHost.Modules.Console;

// ReSharper disable once ClassNeverInstantiated.Global
public class LocalConsoleService(
    IShellManager shellManager, LocalConsole console, ConsoleSettings config) : IDisposable
{
    private IDisposable? _disposable;

    public void Start()
    {
        if (!config.EnableLocalConsole) return;
        _disposable = shellManager.StartShell(console, "local-shell");
    }

    public void Dispose() => _disposable?.Dispose();
}