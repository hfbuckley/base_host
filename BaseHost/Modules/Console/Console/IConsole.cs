﻿namespace BaseHost.Modules.Console.Console;

public interface IConsole
{
    string ReadLine();
    void ClearConsole();
    void WriteLine(string str);
    void Write(string str);
}