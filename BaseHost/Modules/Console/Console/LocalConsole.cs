﻿namespace BaseHost.Modules.Console.Console;

public class LocalConsole : IConsole
{
    public void ClearConsole() => System.Console.Clear();
    public void WriteLine(string str) => System.Console.WriteLine(str);
    public void Write(string str) => System.Console.Write(str);
    public string ReadLine() => System.Console.ReadLine()!;
}