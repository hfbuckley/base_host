namespace BaseHost.Modules.Console.Console;

public class SimpleConsole(Func<string> read, Action<string> write) : IConsole
{
    public string ReadLine() => read();

    public void ClearConsole() { }

    public void WriteLine(string str) => write($"{str}{Environment.NewLine}");

    public void Write(string str) => write(str);
}