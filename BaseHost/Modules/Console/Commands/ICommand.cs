﻿using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console.Commands;

public interface ICommand
{
    void Start(IConsole console, string [] args);
}