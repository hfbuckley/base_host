﻿using Autofac;
using BaseHost.Extensions;
using BaseHost.Modules.Console.Manager;

namespace BaseHost.Modules.Console.Commands;

public class SystemCommandBootstrapper(ICommandManager commandManager) : IStartable
{
    public void Start()
    {
        var args = new OutputOnlyCommand(Environment.GetCommandLineArgs().Skip(1).JoinString(" "));
        var shutdown = new Shutdown();
            
        commandManager.Register("get-starting-args",args);
        commandManager.Register("shutdown",shutdown);
    }
}