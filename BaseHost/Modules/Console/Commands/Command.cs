using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console.Commands;

public class Command(Action<IConsole> action) : GeneralCommand
{
    protected override void Start() => action(Console);
}
    
public class Command<T>(Action<IConsole, T> action) : GeneralCommand<T>
{
    protected override void Start(T arg) => action(Console,arg);
}