﻿namespace BaseHost.Modules.Console.Commands;

public class OutputOnlyCommand(Func<string> output) : GeneralCommand
{
    public OutputOnlyCommand(string output) :this(() =>output){ }
    protected override void Start() => WriteLine(output());
}