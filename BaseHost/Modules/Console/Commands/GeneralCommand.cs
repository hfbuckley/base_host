using BaseHost.Modules.Console.Console;
using CommandLine;
using CommandLine.Text;

namespace BaseHost.Modules.Console.Commands;

public abstract class GeneralCommand : BaseCommand
{
    protected abstract void Start();
    public override void Start(IConsole console, string[] args)
    {
        base.Start(console, args);
        if (args.Length != 0)
        {
            console.WriteLine("Arguments are not supported by this command");
            return;
        }
        Start();
    }
}
    
public abstract class GeneralCommand<T> : BaseCommand
{
    protected abstract void Start(T argument);
    public override void Start(IConsole console, string[] args)
    {
        base.Start(console,args);
        var parserResult = new Parser(s =>s.HelpWriter = null)
            .ParseArguments<T>(args);
        parserResult.WithParsed(Start)
            .WithNotParsed(_ =>
                console.WriteLine(HelpText.AutoBuild(parserResult,
                    h => HelpText.DefaultParsingErrorsHandler(parserResult, h), e => e)));
    }
}