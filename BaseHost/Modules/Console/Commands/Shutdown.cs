﻿using CommandLine;
using JetBrains.Annotations;

namespace BaseHost.Modules.Console.Commands;

public class Shutdown : GeneralCommand<Shutdown.Options>
{
    public class Options
    {
        [Option('f', "force", Required = false, HelpText = "shutdown without asking for confirmation")]
        [PublicAPI]
        public bool Force { get; set; }
    }

    protected override void Start(Options argument)
    {
        if (argument.Force)
        {
            WriteLine("Shutting down...");
            Environment.Exit(0);
        }
        else
        {
            WriteLine($"This will shutdown the process {Environment.NewLine}Are you sure you want to continue? y/N");
            while (ReadLine().ToUpper() == "Y")
            {
                WriteLine("Shutting down...");
                Environment.Exit(0);
            }
        }
    }
}