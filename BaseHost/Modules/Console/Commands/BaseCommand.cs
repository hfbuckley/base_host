using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console.Commands;

public abstract class BaseCommand: ICommand
{
    protected IConsole Console = null!;

    protected string ReadLine() => Console.ReadLine();
    protected void WriteLine(string s) => Console.WriteLine(s);
    protected void Write(string s) => Console.Write(s);
    protected void ClearConsole() => Console.ClearConsole();
        
    public virtual void Start(IConsole console, string[] args)
    {
        Console = console;
    }
}