﻿using Autofac;
using BaseHost.Extensions;
using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console;

public class LocalTerminalModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<LocalConsole>().SingleInstance();
        builder.AutoActivateWith<LocalConsoleService>(i => i.Start());
    }
}