﻿using BaseHost.Modules.Console.Console;
using BaseHost.Modules.Console.Manager;

namespace BaseHost.Modules.Console;

public class Shell(ICommandManager commandManager, ConsoleSettings config)
{
    public void Start(IConsole console)
    {
        console.ClearConsole();
        console.WriteLine(config.WelcomeMessage);
        do
        {
            console.Write(config.ShellPrompt);
            var input = console.ReadLine();
            commandManager.FindCommand(GetCmd(input))
                .Start(console,GetArgs(input));
        } while (true);
        // ReSharper disable once FunctionNeverReturns
    }

    private static string GetCmd(string input) => input.Split(' ')[0];

    private static string[] GetArgs(string input)
    {
        var line = input.Split(' ');
        return line.Length > 1
            ? line.Skip(1).ToArray()
            : [];
    }
}