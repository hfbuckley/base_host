using JetBrains.Annotations;

namespace BaseHost.Modules.Console.Cli;

[PublicAPI]
public class Menu<T>(string? title = null, string? content = null)
{
    public readonly IList<Option<T>> Options = new List<Option<T>>();

    public string Title { get; } = title!;
    public string Content { get; } = content!;

    public Menu<T> AddOption(string description, T value)
    {
        Options.Add(new Option<T>.ValueOption(description, value));
        return this;
    }
        
    public Menu<T> AddOption(string description, Action<Menu<T>> addOptions)
        => AddOption(description, string.Empty, addOptions);

    public Menu<T> AddOption(string description, string content, Action<Menu<T>> addOptions)
    {
        var option = new Option<T>.MenuOption(description, new Menu<T>(description, content));
        addOptions(option.Menu);
        Options.Add(option);
        return this;
    }
}