using BaseHost.Extensions;
using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console.Cli;

public class MenuController<T>
{
    private Menu<T> CurrentMenu => _navigation.Peek();
    private readonly IConsole _console;
    private readonly Stack<Menu<T>> _navigation = new();
        
    public MenuController(IConsole console, Menu<T> menu)
    {
        _console = console;
        _navigation.Push(menu);
    }
    public T GetSelection()
    {
        while (true)
        {
            _console.ClearConsole();
            _console.WriteLine(CurrentMenu.Content);
            CurrentMenu.Options
                .Select((s,i) => $"{i+1} - {s.Description}")
                .ForEach(_console.WriteLine);
            var numOfOptions = CurrentMenu.Options.Count;
            if (_navigation.Count > 1)
            {
                numOfOptions++;
                _console.WriteLine($"{numOfOptions} - go back");
                _console.WriteLine(_navigation.Reverse().Select(i => i.Title).JoinString("-> "));
            }
            _console.Write($"Enter your selection (1-{numOfOptions}): ");

            if(!int.TryParse(_console.ReadLine(), out var number) 
               || number < 1 
               || number >= numOfOptions+1) continue;

            if (number > CurrentMenu.Options.Count)
            {
                _navigation.Pop();
                continue;
            }
            switch (CurrentMenu.Options[number-1])
            {
                case Option<T>.ValueOption valueOption:
                    return valueOption.Value;
                case Option<T>.MenuOption menuOption:
                    _navigation.Push(menuOption.Menu);
                    break;
            }
        }
    }
}