namespace BaseHost.Modules.Console.Cli;

public abstract class Option<T>
{
    public string Description { get; private set; } = "Not Set";
        
    public class MenuOption : Option<T>
    {
        public MenuOption(string description, Menu<T> menu)
        {
            Description = description;
            Menu = menu;
        }
        public Menu<T> Menu { get; }
    }
        
    public class ValueOption : Option<T>
    {
        public ValueOption(string description, T value)
        {
            Description = description;
            Value = value;
        }
        public T Value { get; }
    }
}