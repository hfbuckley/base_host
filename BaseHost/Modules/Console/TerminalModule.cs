﻿using Autofac;
using BaseHost.Extensions;
using BaseHost.Modules.Console.Commands;
using BaseHost.Modules.Console.Manager;

namespace BaseHost.Modules.Console;

public class TerminalModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        builder.RegisterType<Shell>();
        builder.RegisterType<CommandManager>().As<ICommandManager>().SingleInstance();
        builder.RegisterType<ShellManager>().As<IShellManager>().SingleInstance();
        builder.AutoActivateWith<SystemCommandBootstrapper>(i => i.Start());
    }
}