﻿using BaseHost.Modules.Console.Commands;

namespace BaseHost.Modules.Console.Manager;

public interface ICommandManager
{
    IDisposable Register(string name,ICommand command);
    ICommand FindCommand(string name);
}