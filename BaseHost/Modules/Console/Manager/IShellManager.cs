﻿using BaseHost.Modules.Console.Console;

namespace BaseHost.Modules.Console.Manager;

public interface IShellManager
{
    IDisposable StartShell(IConsole console, string? id = null);
}