﻿using System.Collections.Concurrent;
using BaseHost.Extensions;
using BaseHost.Modules.Console.Commands;

namespace BaseHost.Modules.Console.Manager;

public class CommandManager : ICommandManager
{
    private static readonly ICommand Fallback = new OutputOnlyCommand("Failed to find command...");
    private readonly ConcurrentDictionary<string, ICommand> _commands = new();

    public CommandManager() 
        => Register("help", new OutputOnlyCommand(() => _commands.Keys.ToArray().JoinString(Environment.NewLine)));
        
    public IDisposable Register(string name, ICommand command)
    {
        _commands.AddOrUpdate(name, command, (_, _) => command);
        return  new Action(() => _commands.TryRemove(name, out _)).ToDisposable();
    }
        
    public ICommand FindCommand(string name) 
        => _commands.TryGetValue(name, out var command) ? command : Fallback;
}