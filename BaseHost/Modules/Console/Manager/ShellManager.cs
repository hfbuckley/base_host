﻿using System.Reactive;
using System.Reactive.Linq;
using BaseHost.Host;
using BaseHost.Modules.Console.Console;
using NLog;

namespace BaseHost.Modules.Console.Manager;

public class ShellManager(Shell shell, ISchedulerProvider scheduler, ILogger logger) : IShellManager
{
    public IDisposable StartShell(IConsole console,string? id = null)
    {
        return Observable.Return(Unit.Default)
            .ObserveOn(scheduler.Ui)
            .Subscribe(_ =>
            {
                try
                {
                    logger.Info($"Starting Console with Id:{id}");
                    shell.Start(console);
                    logger.Info($"Console exited with Id:{id}");
                }
                catch (Exception e)
                {
                    logger.Error(e,$"Console exited (Id:{id}) with error");

                }
            });
    }
}