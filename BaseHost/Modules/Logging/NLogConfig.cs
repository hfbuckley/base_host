using NLog;
using NLog.Config;
using NLog.Targets;

namespace BaseHost.Modules.Logging;

public static class NLogConfig
{
    private static LogLevel Map(string logLevel)
    {
        return logLevel.ToLower() switch
        {
            "trace" => LogLevel.Trace,
            "debug" => LogLevel.Debug,
            "info" => LogLevel.Info,
            "warn" => LogLevel.Warn,
            "error" => LogLevel.Error,
            _ => LogLevel.Fatal
        };
    }
    
    public static void Setup(string location,string console, string file)
    {
        var configuration = new LoggingConfiguration();
        var logfile = new FileTarget("logfile")
        {
            FileName = location,
            Layout = "${longdate}|${level:uppercase=true}|${logger}|${message} ${exception:format=tostring}"
        };
        var logconsole = new ColoredConsoleTarget("logconsole")
        {
            Layout = "${time}|${logger:shortName=true}|${message} ${exception:format=tostring}",
            EnableAnsiOutput = true
        };
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Trace",
            ConsoleOutputColor.Black, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Debug", 
            ConsoleOutputColor.DarkGray, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Info",
            ConsoleOutputColor.Gray, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Debug",
            ConsoleOutputColor.DarkGray, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Warn",
            ConsoleOutputColor.Yellow, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Error",
            ConsoleOutputColor.Red, ConsoleOutputColor.NoChange));
        logconsole.RowHighlightingRules.Add(new ConsoleRowHighlightingRule("level == LogLevel.Fatal",
            ConsoleOutputColor.Red, ConsoleOutputColor.White));
        
        configuration.AddRule(LogLevel.Info, LogLevel.Fatal, logfile,"Microsoft.Hosting.Lifetime", true);
        configuration.AddRule(LogLevel.Trace, LogLevel.Info, logfile,"Microsoft.*",true);
        
        configuration.AddRule(Map(console), LogLevel.Fatal, logconsole);
        configuration.AddRule(Map(file), LogLevel.Fatal, logfile);

        configuration.AddTarget(logconsole);
        configuration.AddTarget(logfile);
        LogManager.Configuration = configuration;
    }
}