﻿using System.Reflection;
using Autofac.Core;
using Autofac.Core.Registration;
using Autofac.Core.Resolving.Pipeline;
using BaseHost.Extensions;
using NLog;
using Module = Autofac.Module;

namespace BaseHost.Modules.Logging;

public class NLogModule : Module
{
    private static readonly IResolveMiddleware NlogMiddleware = new NLogResolveMiddleware();
    protected override void AttachToComponentRegistration(IComponentRegistryBuilder componentRegistryBuilder, IComponentRegistration registration)
        => registration.PipelineBuilding += (_, pipeline) => pipeline.Use(NlogMiddleware);
    
    private class NLogResolveMiddleware : IResolveMiddleware
    {
        public PipelinePhase Phase => PipelinePhase.ParameterSelection;

        public void Execute(ResolveRequestContext context, Action<ResolveRequestContext> next)
        {
            context.ChangeParameters(context.Parameters.Union(
                new[]
                {
                    new ResolvedParameter(
                        (p, _) => p.ParameterType == typeof(ILogger),
                        (p, _) => LogManager.GetLogger(p.Member.DeclaringType!.ToFriendlyString()))
                }));
                
            next(context);
            
            if (!context.NewInstanceActivated) return;
            {
                var instanceType = context.Instance!.GetType();
#pragma warning disable CS0618 // Type or member is obsolete
                var logger = LogManager.GetLogger(instanceType.ToFriendlyString(), null);
#pragma warning restore CS0618 // Type or member is obsolete
                instanceType
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Where(p => p.PropertyType == typeof(ILogger)
                                && p.CanWrite && p.GetIndexParameters().Length == 0)
                    .ForEach(propToSet
                        => propToSet.SetValue(context.Instance, logger));
            }
        }
    }

}