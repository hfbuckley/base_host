namespace BaseHost.Modules.Config;

[AttributeUsage(AttributeTargets.Class, Inherited = false)]
public class ConfigAttribute(string path) : Attribute
{
    public string Path { get; } = path;
}