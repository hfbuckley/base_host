using System.Linq.Expressions;
using System.Reflection;
using Autofac;
using BaseHost.Extensions;
using BaseHost.Modules.Console;
using Microsoft.Extensions.Configuration;
using Module = Autofac.Module;

namespace BaseHost.Modules.Config;

public class ConfigModule : Module
{
    protected override void Load(ContainerBuilder builder)
    {
        AppDomain.CurrentDomain.GetAssemblies()
            // have to manually register the config from this type, some problems loading the types from this assemble
            .Where(a => a != GetType().Assembly)
            .SelectMany(i =>
            {
                try { return i.GetTypes(); }catch { return []; }
            })
            .Concat([
                typeof(ConsoleSettings)
            ])
            .Select(type => (Type: type, type.GetCustomAttributes<ConfigAttribute>().FirstOrDefault()?.Path))
            .Where(i=> i.Path!=null)
            .OfType<(Type Type, string Path)>()
            .ForEach(setting =>
                {
                    builder.Register(i =>
                        {
                            var config = i.Resolve<IConfiguration>();
                            return config.GetSection(setting.Path).Get(setting.Type)
                                   ??
                                   CreateInstanceBinder(setting.Type);
                        })
                        .As(setting.Type);
                }
            );
    }
    
    private static object CreateInstanceBinder(Type type)
    {
        var constructor = type.GetConstructors().FirstOrDefault(i => i.IsPublic);
        
        constructor.ThrowIfNull($"{type.Name} Does not an an appropriate public constructor");
        var parameters = constructor!.GetParameters();
        if(parameters.Length==0) return Expression.New(constructor);

        if (parameters.All(i => i.HasDefaultValue))
            return Activator.CreateInstance(type,parameters.Select(i=> i.DefaultValue).ToArray());

        var missing = parameters.Where(i => !i.HasDefaultValue).Select(i => i.Name).JoinString(",");
        throw new Exception($"{type.Name} constructor doesn't have a default value for {missing}");
    }
}