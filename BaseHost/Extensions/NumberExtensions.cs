namespace BaseHost.Extensions;

public static class NumberExtensions
{
    public static int InRange(this int value, int min, int max) 
        => Math.Min(Math.Max(min, value), max);
    public static float InRange(this float value, float min, float max) 
        => Math.Min(Math.Max(min, value), max);
    public static double InRange(this double value, double min, double max) 
        => Math.Min(Math.Max(min, value), max);
    public static decimal InRange(this decimal value, decimal min, decimal max) 
        => Math.Min(Math.Max(min, value), max);
}