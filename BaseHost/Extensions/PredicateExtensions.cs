namespace BaseHost.Extensions;

public static class PredicateExtensions
{
    public static Predicate<T> Not<T>(this Predicate<T> self) => v=> !self(v);
    public static Predicate<T> And<T>(this Predicate<T> self, Predicate<T> condition) => v=> self(v) && condition(v);
    public static Predicate<T> Xor<T>(this Predicate<T> self, Predicate<T> condition) => v=> self(v) != condition(v);
    public static Predicate<T> Or<T>(this Predicate<T> self, Predicate<T> condition) => v=> self(v) || condition(v);
}