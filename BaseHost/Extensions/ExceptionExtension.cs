using System.Text.RegularExpressions;

namespace BaseHost.Extensions;

public static class ExceptionExtension
{
    public static void ThrowIfNull<T>(this T self, string? error = null)
    {
        if(self == null) throw new FailedConditionException(error??"Value must not be null");
    }
        
    public static void ThrowIf<T>(this T self, Predicate<T> condition, string? error = null)
    {
        if(condition(self)) throw new FailedConditionException(error??"Condition must pass");
    }
        
    public static void ThrowIfEmpty<T>(this IEnumerable<T> self, string? error = null)
    {
        if(self == null) throw new FailedConditionException(error??"Value must not be null");
        if(!self.Any()) throw new FailedConditionException(error??"Value must not be empty");
    }

    public static void ThrowIfTrue(this bool self, string? error = null)
    {
        if(self) throw new FailedConditionException(error??"Value must be true");
    }
        
    public static void ThrowIfFalse(this bool self, string? error= null)
    {
        if(self == false) throw new FailedConditionException(error??"Value must be false");
    }


    private class FailedConditionException(string message) : Exception(message)
    {
        public override string StackTrace
        {
            get
            {
                var lines = Regex.Split(base.StackTrace, "\r\n|\r|\n").Skip(1);
                return string.Join(Environment.NewLine, lines.ToArray());
            }
        }
    }
}