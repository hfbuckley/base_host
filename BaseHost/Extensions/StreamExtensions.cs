﻿using Newtonsoft.Json;

namespace BaseHost.Extensions;

public static class StreamExtensions
{
    public static T? DeserializeFromStream<T>(this Stream stream)
    {
        var serializer = new JsonSerializer();
        using var sr = new StreamReader(stream);
        using var jsonTextReader = new JsonTextReader(sr);
        return serializer.Deserialize<T>(jsonTextReader);
    }
}