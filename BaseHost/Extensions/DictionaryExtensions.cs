namespace BaseHost.Extensions;

public static class DictionaryExtensions
{
    public static IDictionary<TKey, TValue> Add<TKey, TValue>(
        this IDictionary<TKey, TValue> self,TKey key, TValue value)
    {
        self[key] = value;
        return self;
    }
        
    public static IDictionary<TKey, T> Filter<TKey,T>(
        this IDictionary<TKey, T> self, Predicate<T> filter)
        => self
            .Where(i => filter(i.Value))
            .ToDictionary(i => i.Key, i => i.Value);
        
    public static IDictionary<TKey, TNew> FilterSelect<TKey,T,TNew>(
        this IDictionary<TKey, T> self, Predicate<T> filter,Func<T,TNew> map)
        => self
            .Where(i => filter(i.Value))
            .Select(i => (i.Key,map(i.Value)))
            .ToDictionary(i => i.Key, i => i.Item2);

    public static IDictionary<TKey, TNew> SelectValue<TKey,T,TNew>(
        this IDictionary<TKey, T> self, Func<T,TNew> map)
        => self
            .Select(i => (i.Key,map(i.Value)))
            .ToDictionary(i => i.Key, i => i.Item2);
        
    public static TValue? GetOrDefault<TKey, TValue>(
        this IDictionary<TKey, TValue> self,TKey key, TValue? defaultValue = default) =>
        self.TryGetValue(key, out var value) ? value : defaultValue;
}