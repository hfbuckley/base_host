﻿using System.Collections.Immutable;

namespace BaseHost.Extensions;

public static class ImmutableExtensions
{
    public static ImmutableArray<T>.Builder AddInline<T>(this ImmutableArray<T>.Builder self, T item ,int? atIndex = null)
    {
        if (atIndex == null || self.Count == atIndex)
            self.Add(item);
        else if (self.Count > atIndex)
            self[atIndex.Value] = item;
        (self.Count < atIndex).ThrowIfTrue($"Index is too big, array is only {self.Count} big");
        return self;
    }

    public static ImmutableHashSet<T>.Builder AddInline<T>(this ImmutableHashSet<T>.Builder self, T item)
    {
        self.Add(item);
        return self;
    }

    public static ImmutableDictionary<TKey, TItem>.Builder AddInline<TKey, TItem>(
        this ImmutableDictionary<TKey, TItem>.Builder self, TKey key, TItem item) where TKey : notnull
    {
        self[key] = item;
        return self;
    }
        
    public static ImmutableArray<T>.Builder UpdateInline<T>(this ImmutableArray<T>.Builder self, int index, Func<T,T> item)
    {
        if (self.Count > index)
            self[index] = item(self[index]);
        (self.Count <= index).ThrowIfTrue($"Index is too big, array is only {self.Count} big");
        return self;
    }
        
    public static ImmutableDictionary<TKey, TItem>.Builder UpdateInline<TKey, TItem>(
        this ImmutableDictionary<TKey, TItem>.Builder self, TKey key, Func<TItem,TItem> item) where TItem: class where TKey : notnull
    {
        if (self.ContainsKey(key))
            self[key] = item(self[key]);
        return self;
    }
}