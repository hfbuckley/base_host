namespace BaseHost.Extensions;

public class SwitchMap<T, TReturn>(T value)
{
    private bool _hasBeenHandled;
    private TReturn? _convertedValue;

    public SwitchMap<T, TReturn> Case(T comparisonValue, Func<TReturn> action)
    {
        if (!AreEqual(value, comparisonValue)) return this;
        _hasBeenHandled = true;
        _convertedValue = action();
        return this;            

    }
        
    public SwitchMap<T, TReturn> Case(T comparisonValue, TReturn value1)
    {
        if (!AreEqual(value, comparisonValue)) return this;
        _hasBeenHandled = true;
        _convertedValue = value1;
        return this;
    }

    public SwitchMap<T, TReturn> Case<TType>(T comparisonValue, Func<TType, TReturn> action) where TType : T
    {
        if (comparisonValue is not TType valueTType) return this;
        _hasBeenHandled = true;
        _convertedValue = action(valueTType);
        return this;
    }

    public TReturn? Reduce(Func<TReturn> action)
        => !_hasBeenHandled ? action() : _convertedValue;

    private static bool AreEqual(T actualValue, T comparisonValue)
        => Equals(actualValue, comparisonValue);
}