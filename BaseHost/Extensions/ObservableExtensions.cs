using System.Reactive.Linq;
using Newtonsoft.Json;

namespace BaseHost.Extensions;

public static class ObservableExtensions
{
    public static IObservable<IList<T>> ScanList<T>(this IObservable<T> self)
    {
        return self.Scan(new List<T>(), (list, item) =>
        {
            list.Add(item);
            return list;
        });
    }

    public static IObservable<T> OnStart<T>(this IObservable<T> observable, Action action)
    {
        return Observable.Defer(() =>
        {
            action();
            return observable;
        });
    }

    public static IObservable<IDictionary<TKey,T>> ScanDictionary<T,TKey>(this IObservable<T> self, Func<T, TKey> keySelect)
        => self.ScanDictionary(keySelect, i => i, _ => false);

    public static IObservable<IDictionary<TKey,TOut>> ScanDictionary<T,TKey,TOut>(
        this IObservable<T> self,
        Func<T, TKey> keySelect,
        Func<T,TOut> valueSelect) 
        => self.ScanDictionary(keySelect, valueSelect, _ => false);

    public static IObservable<IDictionary<TKey,TOut>> ScanDictionary<T,TKey,TOut>(
        this IObservable<T> self,
        Func<T, TKey> keySelect,
        Func<T,TOut> valueSelect,
        Predicate<T> shouldRemove)
    {
        return self.Scan(new Dictionary<TKey,TOut>(), (dict, item) =>
        {
            var key = keySelect(item);
            var value = valueSelect(item);
            var remove = shouldRemove(item);
            dict[key] = value;
            if (remove)
            {
                dict.Remove(key);
            }
            return dict;
        });
    }
        
    public static IObservable<T> WhenOfType<T>(this IObservable<string> stringObs, JsonConverter[]? settings = null)
        => stringObs
            .Select(i =>
            {
                try
                {
                    return settings != null 
                        ? JsonConvert.DeserializeObject<T>(i, settings)
                        : JsonConvert.DeserializeObject<T>(i);
                }
                catch
                {
                    return default;
                }
            })
            .Where(i => i!=null)
            .Select(i=> i!);
}