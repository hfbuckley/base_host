namespace BaseHost.Extensions;

public static class BoolExtensions
{
    public static bool Not(this bool self) => !self;
    public static bool And(this bool self, bool condition) => self && condition;
    public static bool Xor(this bool self, bool condition) => self != condition;
    public static bool Or(this bool self, bool condition) => self || condition;

    public static bool If(this bool self, Action? whenTrue, Action? whenFalse)
        => self && whenTrue != null
            ? whenTrue.AsFunc(true)()
            : whenFalse != null
                ? whenFalse.AsFunc(false)()
                : self;
    public static bool WhenTrue(this bool self, Action whenTrue)
        => self && whenTrue.AsFunc(true)();
    public static bool WhenFalse(this bool self, Action whenFalse)
        =>  self || whenFalse.AsFunc(false)();
}