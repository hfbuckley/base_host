﻿using System.Text;

namespace BaseHost.Extensions;

public static class EnumerableExtensions
{
    public static string JoinString(this IEnumerable<string> self, string separator)
    {
        var list = self.ToList();
        if (list.Count < 2) return list.FirstOrDefault() ?? string.Empty;

        var sb = new StringBuilder();
        sb.Append(list.FirstOrDefault());
        list.Skip(1).ToList().ForEach(w =>
        {
            sb.Append(separator);
            sb.Append(w);
        });
        return sb.ToString();
    }

    public static void ForEach<T>(this IEnumerable<T> self, Action<T> action)
    {
        foreach (var item in self)
        {
            action(item);
        }
    }
    public static void ForEach<T>(this IEnumerable<T> self, Action<T,int> action)
    {
        var s = self.ToArray();
        for(var i =0;i< s.Length; i++)
        {
            action(s[i],i);
        }
    }

    public static bool TrySingle<T>(this IEnumerable<T> self, out T? single) where T : class
    {
        single = self.SingleOrDefault();
        return single != null;
    }
    
    public static IEnumerable<T> Yield<T>(this T self)
    {
        yield return self;
    }

    public static IEnumerable<T> Concat<T>(this T self, T item)
        => Enumerable.Concat(self.Yield(),item.Yield());
        
    public static IEnumerable<T> Concat<T>(this IEnumerable<T> self, T item)
        => Enumerable.Concat(self,item.Yield());

    public static IEnumerable<TResult> CombineLatest<T,TResult>(
        this IEnumerable<Func<T>> functions, 
        Func<IEnumerable<T>, IEnumerable<TResult>> selectResult)
        =>selectResult(functions.AsParallel().Select(i => i()).ToList());

    public static bool TryGet<T>(
        this T[] self,
        int index, out T? value)
    {
        if (self.Length > index)
        {
            value = self[index];
            return true;
        }
        value = default;
        return false;
    }

    public static ISet<T> ToSet<T>(this IEnumerable<T> self)
        => new HashSet<T>(self);        
    public static Stack<T> ToStack<T>(this IEnumerable<T> self)
        => new(self);
    public static Queue<T> ToQueue<T>(this IEnumerable<T> self)
        => new(self);
}