﻿namespace BaseHost.Extensions;

public static class TypeExtensions
{
    public static string ToFriendlyString(this Type type)
    {
        var typeName = type.Namespace + "." + type.Name;
        var args = type.GetGenericArguments();
        if (!args.Any()) return typeName;

        typeName = typeName.Remove(typeName.LastIndexOf("`", StringComparison.Ordinal));

        return $"{typeName}<{string.Join(", ", args.Select(ToFriendlyStringWithNamespace))}>";
    }
        
    public static string ToFriendlyStringWithoutGeneric(this Type type)
    {
        var typeName = type.Namespace + "." + type.Name;
        var args = type.GetGenericArguments();
        return !args.Any() 
            ? typeName 
            : typeName.Remove(typeName.LastIndexOf("`", StringComparison.Ordinal));
    } 

    public static string ToFriendlyStringWithNamespace(this Type type)
    {
        var typeName = type.Name;
        var args = type.GetGenericArguments();
        if (!args.Any()) return typeName;

        typeName = typeName.Remove(typeName.LastIndexOf("`", StringComparison.Ordinal));

        return $"{typeName}<{string.Join(", ", args.Select(ToFriendlyStringWithNamespace))}>";
    }

    public static string ToFriendlyStringNoNamespace(this Type type)
    {
        var typeName = type.Name;
        var args = type.GetGenericArguments();
        if (!args.Any()) return typeName;

        typeName = typeName.Remove(typeName.LastIndexOf("`", StringComparison.Ordinal));

        return $"{typeName}<{string.Join(", ", args.Select(ToFriendlyStringNoNamespace))}>";
    }
}