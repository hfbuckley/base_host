﻿using System.ComponentModel.DataAnnotations;

namespace BaseHost.Extensions;

public static class ValidationExtensions
{
    private static bool TryValidate<T>(this T self, out ICollection<ValidationResult> results) where T: notnull
    {
        var context = new ValidationContext(self,null,null);
        results = [];
        return Validator.TryValidateObject(self, context, results, true);
    }

    public static T GetValidatedInstance<T>(this T self) where T: notnull
        =>  self.TryValidate(out var validation)
            ? self
            : throw new Exception($"Validation of {self.GetType().Name} Failed : " + validation
                .Select(i=> i.ErrorMessage).JoinString(","));
}