using System.Reactive;
using System.Reactive.Linq;

namespace BaseHost.Extensions;

public static class FunctionExtensions
{
    public static IObservable<T> AsObservable<T>(this Func<T> function)
        => Observable.Defer(() => Observable.Return(function()));
    public static IObservable<Unit> AsObservable(this Action action)
        => AsObservable(action.AsFunc(Unit.Default));
    public static Func<T, bool> AsFunc<T>(Predicate<T> predicate)
        =>v => predicate(v);
    public static Predicate<T> AsPredicate<T>(Func<T, bool> predicate)
        =>v => predicate(v);
    public static Func<T> AsFunc<T>(this Action action, T returnVal)
        => () =>
        {
            action();
            return returnVal;
        };
}