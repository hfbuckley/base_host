using Newtonsoft.Json;

namespace BaseHost.Extensions;

public static class ObjectExtensions
{
    private const string Null = "{null}";
    private static readonly JsonSerializerSettings JsonSettings = new()
    {
        TypeNameHandling = TypeNameHandling.None,
        NullValueHandling = NullValueHandling.Include
    };

    public static TOut? SafeMap<TIn, TOut>(this TIn value, Func<TIn, TOut> map, TOut? fallback = default)
    {
        try
        {
            return map(value);
        }
        catch
        {
            return fallback;
        }
    }
    
    public static bool Try<T>(this T self, Func<T,(bool,T?)> func, out T output)
    {
        output = self;
        try
        {
            var (success, result) = func(self);
            if (success) output = result!;
            return success;
        }
        catch
        {
            return false;
        }
    }
    
    public static bool Try<T>(this T self, Func<T,T?> func, out T output) where T : class
    {
        output = self;
        var result = func(self);
        try
        {
            if (result == default || result == self)
            {
                output = self;
                return false;
            }
            output = result;
            return true;
        }
        catch
        {
            return false;
        }
    }

    public static string ToJson(this object? value, 
        Formatting formatting = Formatting.None,
        JsonSerializerSettings? settings = null) =>
        value == null 
            ? Null
            : JsonConvert.SerializeObject(value, formatting,settings??JsonSettings);
}