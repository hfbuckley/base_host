using BaseHost.Utility;

namespace BaseHost.Extensions;

public static class EnumExtensions
{
    public static T ToMask<T>(this IEnumerable<T> values) where T : Enum
    {
        var builtValue = Enum.GetValues(typeof(T)).Cast<T>()
            .Where(values.Contains)
            .Aggregate(0, (current, value) => current | Convert.ToInt32(value));
        return (T)Enum.Parse(typeof(T), builtValue.ToString());
    }
        
    public static IEnumerable<T> ToValues<T>(this T flags) where T : Enum
        =>Enum.GetValues(typeof(T)).Cast<T>().Where(v => flags.HasFlag(v));
    
    public static T GetRandom<T>() where T: Enum
    {
        var values = Enum.GetValues(typeof(T));
        return (T) values.GetValue(Utils.Random.Next(values.Length));
    }
        
    public static T GetRandom<T>(this T _) where T: Enum
    {
        var values = Enum.GetValues(typeof(T));
        return (T) values.GetValue(Utils.Random.Next(values.Length));
    }
    
    public static T GetOtherRandomValue<T>(this T self) where T: Enum
    {
        var values = Enum.GetValues(typeof(T)).OfType<T>().Where(i=> !i.Equals(self)).ToArray();
        return (T) values.GetValue(Utils.Random.Next(values.Length));
    }
}