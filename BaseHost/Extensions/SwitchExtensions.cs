﻿namespace BaseHost.Extensions;

public static class SwitchExtensions
{
    public static Switch<T> ToSwitch<T>(this T value) 
        => new(value);

    public static SwitchMap<T, TReturn> ToSwitchMap<T, TReturn>(this T value) 
        => new(value);
}