namespace BaseHost.Extensions;

public class Switch<T>(T value)
{
    private bool _hasBeenHandled;

    public Switch<T> Case(T comparisonValue, Action action)
    {
        if (!AreEqual(value, comparisonValue)) return this;
        _hasBeenHandled = true;
        action();
        return this;
    }

    public Switch<T> Case<TType>(T comparisonValue, Action<TType> action) where TType : T
    {
        if (comparisonValue is not TType valueTType) return this;
        _hasBeenHandled = true;
        action(valueTType);
        return this;
    }

    public void Default(Action action)
    {
        if (!_hasBeenHandled) action();
    }

    private static bool AreEqual(T actualValue, T comparisonValue)
        => Equals(actualValue, comparisonValue);
}