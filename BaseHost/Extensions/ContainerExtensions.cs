﻿using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Core.Registration;

namespace BaseHost.Extensions;

public static class ContainerExtensions
{
    public static IRegistrationBuilder<T, ConcreteReflectionActivatorData, SingleRegistrationStyle>
        AutoActivateWith<T>(
            this IRegistrationBuilder<T,
                ConcreteReflectionActivatorData, SingleRegistrationStyle> self,
            Action<T> bootstrap) 
        => self
            .OnActivated(i => bootstrap(i.Instance))
            .AutoActivate();


    public static IRegistrationBuilder<T, ConcreteReflectionActivatorData, SingleRegistrationStyle>
        AutoActivateWith<T>(this ContainerBuilder self,
            Action<T> bootstrap) where T : notnull
        => self
            .RegisterType<T>()
            .AutoActivateWith(bootstrap);
    
    public static ISourceRegistrar ConfigureContainer(this ContainerBuilder builder, IComponentContext context)
        => builder.RegisterSource(new CrossContainerSource(context));
    
    private class CrossContainerSource(IComponentContext context) : IRegistrationSource
    {
        public IEnumerable<IComponentRegistration> RegistrationsFor(
            Service service, Func<Service, IEnumerable<ServiceRegistration>> _)
            => context.ComponentRegistry.RegistrationsFor(service);
        public bool IsAdapterForIndividualComponents => false;
    }
}