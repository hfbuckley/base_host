﻿using System.Reactive.Disposables;

namespace BaseHost.Extensions;

public static class DisposableExtensions
{
    public static CompositeDisposable AddToDisposable(this IDisposable self, CompositeDisposable disposable)
    {
        disposable.Add(self);
        return disposable;
    }

    public static CompositeDisposable AddAction(this CompositeDisposable self, Action action)
    {
        self.Add(action.ToDisposable());
        return self;
    }

    public static IDisposable ToDisposable(this Action self) => Disposable.Create(self);
}