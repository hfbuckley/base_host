using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BaseHost.Contract;

public class EnumDefaultValueContractResolver : CamelCasePropertyNamesContractResolver
{
    public EnumDefaultValueContractResolver()
    {
        NamingStrategy = new SnakeCaseNamingStrategy();
    }
    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        var property = base.CreateProperty(member, memberSerialization);
        if (property.DefaultValueHandling == null && property.PropertyType?.IsEnum == true)
            property.DefaultValueHandling = DefaultValueHandling.Include;
        else
            property.DefaultValueHandling = DefaultValueHandling.Ignore;
        return property;
    }
}