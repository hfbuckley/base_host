﻿using System.Text;
using BaseHost.Extensions;
using BaseHost.Generators.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace BaseHost.Generators.Collection;

[Generator]
#pragma warning disable RS1042
public class Generator : ISourceGenerator
#pragma warning restore RS1042
{
    private static readonly string BaseHostVersion = typeof(Generator).Assembly.GetName().Version.ToString();
    
    public void Initialize(GeneratorInitializationContext context) { }

    public void Execute(GeneratorExecutionContext context)
    {
        foreach (var classDeclaration in context.GetClassesWithAttribute<CollectionAttribute>())
        {
            var attributeArgs = classDeclaration.GetAttributeValues<CollectionAttribute>(context);
            var size = attributeArgs.GetOrDefault(nameof(CollectionAttribute.Size)) as int?;
            var type = attributeArgs.GetOrDefault(nameof(CollectionAttribute.Type)) as string;
            var sourceBuilder = CreateSourceFileWithImports(classDeclaration);
            var className = classDeclaration.Identifier.ToString();
            sourceBuilder.Append(GenerateBuilder(classDeclaration,null,className,type,size??4));
            context.AddFormattedSource($"{className}.Collection.generated", sourceBuilder.ToString());
        }
        foreach (var recordDeclaration in context.GetRecordsWithAttribute<CollectionAttribute>())
        {
            var attributeArgs = recordDeclaration.GetAttributeValues<CollectionAttribute>(context);
            var size = attributeArgs.GetOrDefault(nameof(CollectionAttribute.Size)) as int?;
            var type = attributeArgs.GetOrDefault(nameof(CollectionAttribute.Type)) as string;
            var sourceBuilder = CreateSourceFileWithImports(recordDeclaration);
            var className = recordDeclaration.Identifier.ToString();
            sourceBuilder.Append(GenerateBuilder(null,recordDeclaration,className,type,size??4));
            context.AddFormattedSource($"{className}.Collection.generated", sourceBuilder.ToString());
        }
    }

    private static StringBuilder CreateSourceFileWithImports(BaseTypeDeclarationSyntax typeDeclaration)
     => typeDeclaration.GetUsingDirectives()
         .Select(usingSyntax => usingSyntax.ToString())
         .Concat("using BaseHost.Extensions;") // for validation extension used inside Build()
         .Concat("using System;") // for Func<>
         .Concat("using System.Linq;")  
         .Concat("using System.Collections;")
         .Concat("using System.CodeDom.Compiler;")
         .Concat("using System.Collections.Immutable;")
         .Concat("using System.Collections.Generic;")
         .Distinct()
         .Aggregate(new StringBuilder(), (builder, s) => builder.Append($"{s}\n"));
    
    private string GenerateBuilder(
        ClassDeclarationSyntax? classSyntax, 
        RecordDeclarationSyntax? recordSyntax,
        string className,
        string? type,
        int size)
    {
        var classOrRecord = classSyntax == null ? "record" : "class";
        var @namespace = classSyntax?.GetNamespace() ?? recordSyntax?.GetNamespace();
            
        return /*lang=c#*/ $$"""

                 #pragma warning disable CS8604
                 #pragma warning disable CS8618
                 #pragma warning disable CS8625
                 #nullable enable

                 namespace {{@namespace}}
                 {
                     public partial {{classOrRecord}} {{className}}
                     {
                        private readonly {{type}}?[] _items = new {{type}}[{{size}}];
                         
                         public {{className}}(){}
                         
                         protected {{className}}({{className}} other){
                             _items = ({{type}}[]) other._items.Clone();
                         }
                         
                         protected {{className}}({{type}}[] array)
                         {
                             if (array.Length > {{size}}) 
                                 throw new Exception("Too many items in array, max is {{size}}");
                             for (var i = 0; i < array.Length; i++)
                                 _items[i] = array[i];
                         }
                         
                         public static implicit operator {{className}}({{type}}[] array) => new(array);
                         
                         public {{type}}? this[int index] => AsEnumerable().Skip(index).FirstOrDefault();
                 
                         {{Enumerable.Range(0,size).Select(i=> GetProperty(type!,i, size)).JoinString("\n")}}
                             
                         public {{className}} WithUpdated(Func<int, {{type}}?, {{type}}?> updateFunc)
                             => this with
                             {
                               {{Enumerable.Range(0,size).Select(i=> $"Slot{i+1} = updateFunc({i}, Slot{i+1})").JoinString(",\n")}}
                             };
                         
                         public {{className}} WithUpdated(int index, Func<{{type}}?, {{type}}?> updateFunc)
                            => WithUpdated((i, item) => i == index ? updateFunc(item) : item!);
                         
                         public IEnumerable<{{type}}> AsEnumerable() => _items.Where(i => i != null)!;
                 
                         public int Count() => AsEnumerable().Count();
                     }
                 }
                 """;
    }

    private static string GetProperty(string type, int index, int max)
        =>/*lang=c#*/ $$"""
                        [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                        [System.ComponentModel.Description("{{type.Split('.').Last()}} Slot {{index+1}} of {{max}}")]
                        public {{type}}? Slot{{index+1}}
                        {
                            get => AsEnumerable().Skip({{index}}).FirstOrDefault();
                            init => _items[{{index}}] = value!;
                        }
                        """; 
    }