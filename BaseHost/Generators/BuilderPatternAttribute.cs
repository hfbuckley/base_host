﻿namespace BaseHost.Generators;

[AttributeUsage(AttributeTargets.Class)]
public class BuilderPatternAttribute : Attribute
{
    public bool MakeBuilderPublic { get; set; } = false;
}

[AttributeUsage(AttributeTargets.Method)]
public class BuilderValidationAttribute :  Attribute;

[AttributeUsage(AttributeTargets.Method)]
public class OnBuildAttribute :  Attribute;

[AttributeUsage(AttributeTargets.Property)]
public class BuilderIgnoreAttribute :  Attribute;
