namespace BaseHost.Generators;

[AttributeUsage(AttributeTargets.Property)]
public class IncludePropertiesAttribute : Attribute;