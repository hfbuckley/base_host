namespace BaseHost.Generators;

[AttributeUsage(AttributeTargets.Class)]
public class CollectionAttribute : Attribute
{
    public string Type { get; set; } = null!;
    public int Size { get; set; } = 4;
}