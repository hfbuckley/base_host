﻿using System.Text;
using BaseHost.Extensions;
using BaseHost.Generators.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace BaseHost.Generators.IncludeProperties;

[Generator]
#pragma warning disable RS1042
public class Generator : ISourceGenerator
#pragma warning restore RS1042
{
    private static readonly string BaseHostVersion = typeof(Generator).Assembly.GetName().Version.ToString();
    
    public void Initialize(GeneratorInitializationContext context) { }

    public void Execute(GeneratorExecutionContext context)
    {
        var typesToExtend = context
            .GetTypesOfPropertiesWithAttribute<IncludePropertiesAttribute>();
     
        foreach (var classDeclaration in typesToExtend)
        {
            
            var imports = classDeclaration.Item2
                .SelectMany(i =>
                    context
                        .GetClasses().Cast<BaseTypeDeclarationSyntax>()
                        .Concat(context.GetRecords())
                        .Where(x => i.Type.ToString() == x.Identifier.ToString()))
                .SelectMany(i => i.GetUsingDirectives()).Distinct();
            
            var sourceBuilder = CreateSourceFileWithImports(classDeclaration.Item1,imports);
            
            var className = classDeclaration.Item1.Identifier.ToString();

            sourceBuilder.Append(GenerateBuilder(
                classDeclaration.Item1,
                classDeclaration.Item2, 
                context,className));
            context.AddFormattedSource($"{className}.MergedProperties.generated", sourceBuilder.ToString());
        }
    }

    private static StringBuilder CreateSourceFileWithImports(
        BaseTypeDeclarationSyntax typeDeclaration, IEnumerable<UsingDirectiveSyntax> usingsStatements)
     => typeDeclaration.GetUsingDirectives()
         .Select(usingSyntax => usingSyntax.ToString())
         .Concat(usingsStatements.Select(usingSyntax => usingSyntax.ToString()))
         .Concat("using BaseHost.Extensions;") // for validation extension used inside Build()
         .Concat("using System;") // for Func<>
         .Concat("using System.Linq;")  
         .Concat("using System.Collections;")
         .Concat("using System.CodeDom.Compiler;")
         .Concat("using System.Collections.Immutable;")
         .Concat("using System.Collections.Generic;")
         .Distinct()
         .Aggregate(new StringBuilder(), (builder, s) => builder.Append($"{s}\n"));


    private record Property(string Name, string Type, string Attributes)
    {
        public string Name { get; } = Name;
        public string Type { get; } = Type;
        public string Attributes { get; } = Attributes;
    }
    
    private static string GenerateBuilder(
        BaseTypeDeclarationSyntax? classSyntax, 
        IEnumerable<PropertyDeclarationSyntax> propertiesToMerge,
        GeneratorExecutionContext context,
        string className)
    {
        var propertiesToAdd = propertiesToMerge
            .Select(property =>
            {
                var type = context.GetClasses().Cast<BaseTypeDeclarationSyntax>()
                    .Concat(context.GetRecords())
                    .FirstOrDefault(o => o.Identifier.ToString() == property.Type.ToString());

                var properties =
                    ((type as ClassDeclarationSyntax)?.GetProperties() ??
                     (type as RecordDeclarationSyntax)?.GetProperties())!
                    .Where(i=> i.HasPublicGetter() && i.IsPublic())
                    .Where(i=> i.GetAttribute<IncludePropertiesAttribute>() == null)
                    .Select(i => new Property(
                            $"{i.Identifier.Text}",
                        i.Type.ToString(),
                        i.AttributeLists.ToString()));
                
                properties = properties.Concat(
                    (type as RecordDeclarationSyntax)?
                    .ParameterList?.Parameters.Select(i =>
                        new Property(i.Identifier.Text,
                            i.Type!.ToString(), 
                            i.AttributeLists.ToString())) ?? []);

                return (property, properties);
            })
            .ToList();

        var values = propertiesToAdd.Select(i => i.properties?
            .Select(property => $"""
                         [GeneratedCode("BaseHost","{BaseHostVersion}")]
                         {property.Attributes}
                         public {property.Type} {property.Name} 
                            => {i.property.Identifier.Text}.{property.Name};
                         """)
            .JoinString("\n") ?? string.Empty)
            .JoinString("\n\n");

        var implicitsOperators = propertiesToAdd.Select(i => $"""
                                     public static implicit operator {i.property.Type}({className} self) 
                                               => self.{i.property.Identifier.Text};
                                     """).JoinString("\n");
        
        
        var classOrRecord = classSyntax is RecordDeclarationSyntax ? "record" : "class";
        var @namespace = classSyntax?.GetNamespace();
        
        return /*lang=c#*/ $$"""

                 #pragma warning disable CS8604
                 #pragma warning disable CS8618
                 #pragma warning disable CS8625
                 #nullable enable

                 namespace {{@namespace}}
                 {
                     public partial {{classOrRecord}} {{className}}
                     {
                         {{implicitsOperators}}
                         {{values}}
                     }
                 }
                 """;
    }
}