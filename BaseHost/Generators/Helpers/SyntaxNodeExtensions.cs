﻿using System.ComponentModel;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace BaseHost.Generators.Helpers;

public static class SyntaxNodeExtensions
{
    private static T? GetParentOfType<T>(this SyntaxNode node) where T : CSharpSyntaxNode
    {
        while (true)
        {
            switch (node.Parent)
            {
                case T parent:
                    return parent;
                case null:
                    return null;
            }
            node = node.Parent;
        }
    }

    public static string GetNamespace(this SyntaxNode node)
        => node.GetParentOfType<NamespaceDeclarationSyntax>()?.Name.ToString() ??
           node.GetParentOfType<FileScopedNamespaceDeclarationSyntax>()?.Name.ToString()!;

    public static IEnumerable<ClassDeclarationSyntax> GetClasses(this GeneratorExecutionContext context)
        => context.Compilation.SyntaxTrees
            .SelectMany(s => s.GetRoot().DescendantNodes())
            .OfType<ClassDeclarationSyntax>();
    
    public static IEnumerable<RecordDeclarationSyntax> GetRecords(this GeneratorExecutionContext context)
        => context.Compilation.SyntaxTrees
            .SelectMany(s => s.GetRoot().DescendantNodes())
            .OfType<RecordDeclarationSyntax>();

    private static IEnumerable<EnumDeclarationSyntax> GetEnums(this GeneratorExecutionContext context)
        => context.Compilation.SyntaxTrees
            .SelectMany(s => s.GetRoot().DescendantNodes())
            .OfType<EnumDeclarationSyntax>();
    
    public static IEnumerable<ClassDeclarationSyntax> GetClassesWithAttribute<T>(this GeneratorExecutionContext context)
        where T: Attribute
        => context.GetClasses()
            .Where(s=> HasAttribute<T>(s.AttributeLists))
            .ToList();
    
    public static IEnumerable<RecordDeclarationSyntax> GetRecordsWithAttribute<T>(this GeneratorExecutionContext context)
        where T: Attribute
        => context.GetRecords()
            .Where(s=> HasAttribute<T>(s.AttributeLists))
            .ToList();
    
    public static IEnumerable<EnumDeclarationSyntax> GetEnumsWithAttribute<T>(this GeneratorExecutionContext context) 
        where T: Attribute
        => context.GetEnums()
            .Where(s=> HasAttribute<T>(s.AttributeLists))
            .ToList();
    
    public static IEnumerable<(ClassDeclarationSyntax,MethodDeclarationSyntax)> GetMethodsWithAttribute<T>(
        this GeneratorExecutionContext context) where T: Attribute
        => context.GetClasses()
            .SelectMany(i=> i.GetMethodsWithAttribute<T>().Select(m=> (i,m)))
            .ToList();
    
    public static IEnumerable<(RecordDeclarationSyntax,MethodDeclarationSyntax)> GetRecordMethodsWithAttribute<T>(
        this GeneratorExecutionContext context) where T: Attribute
        => context.GetRecords()
            .SelectMany(i=> i.GetMethodsWithAttribute<T>().Select(m=> (i,m)))
            .ToList();
    
    
        
    public static IEnumerable<(ClassDeclarationSyntax, List<PropertyDeclarationSyntax>)> 
        GetClassesOfPropertiesWithAttribute<T>(
            this GeneratorExecutionContext context) where T: Attribute
    {
        return context.GetClasses()
            .Select(c=> (c,GetPropertiesWithAttribute<T>(c).ToList()))
            .Where(i=> i.Item2.Any())
            .ToList();
    }
    
    public static IEnumerable<(BaseTypeDeclarationSyntax, List<PropertyDeclarationSyntax>)> 
        GetTypesOfPropertiesWithAttribute<T>(
            this GeneratorExecutionContext context) where T: Attribute
    {
        return context.GetRecords()
            .Select(c=> ((BaseTypeDeclarationSyntax)c,GetPropertiesWithAttribute<T>(c).ToList()))
            .Where(i=> i.Item2.Any())
            .Concat(context.GetClasses()
                    .Select(c=> ((BaseTypeDeclarationSyntax)c,GetPropertiesWithAttribute<T>(c).ToList()))
                    .Where(i=> i.Item2.Any()))
            .ToList();
    }
        
    public static IEnumerable<(ClassDeclarationSyntax, List<FieldDeclarationSyntax>)> 
        GetClassesOfFieldsWithAttribute<T>(
            this GeneratorExecutionContext context) where T: Attribute
    {
        return context.GetClasses()
            .Select(c=> (c,GetFieldsWithAttribute<T>(c).ToList()))
            .Where(i=> i.Item2.Any())
            .ToList();
    }

    public static IEnumerable<MethodDeclarationSyntax> GetMethodsWithAttribute<T>(this ClassDeclarationSyntax self)
        where T: Attribute
        => self.Members
            .OfType<MethodDeclarationSyntax>()
            .Where(i => i.AttributeLists.HasAttribute<T>());

    public static IEnumerable<MethodDeclarationSyntax> GetMethodsWithAttribute<T>(this RecordDeclarationSyntax self)
        where T: Attribute
        => self.Members
            .OfType<MethodDeclarationSyntax>()
            .Where(i => i.AttributeLists.HasAttribute<T>());

    
    public static IEnumerable<PropertyDeclarationSyntax> GetProperties(this ClassDeclarationSyntax self) 
        => self.Members.OfType<PropertyDeclarationSyntax>();
    public static IEnumerable<PropertyDeclarationSyntax> GetProperties(this RecordDeclarationSyntax self) 
        => self.Members.OfType<PropertyDeclarationSyntax>();
    
    public static IEnumerable<PropertyDeclarationSyntax> GetPropertiesWithAttribute<T>(
        this ClassDeclarationSyntax self) where T: Attribute
        => self.GetProperties().Where(i => i.AttributeLists.HasAttribute<T>());
    
    private static IEnumerable<PropertyDeclarationSyntax> GetPropertiesWithAttribute<T>(
        this RecordDeclarationSyntax self) where T: Attribute
        => self.GetProperties().Where(i => i.AttributeLists.HasAttribute<T>());

    private static IEnumerable<FieldDeclarationSyntax> GetFieldsWithAttribute<T>(
        this ClassDeclarationSyntax self) where T: Attribute
    {
        return self.Members.OfType<FieldDeclarationSyntax>()
            .Where(i => i.AttributeLists.HasAttribute<T>());
    }
        
    public static string? GetDescription(this MemberDeclarationSyntax prop,GeneratorExecutionContext context)
    {
        var semanticModel = context.Compilation.GetSemanticModel(prop.SyntaxTree);
        var expression = GetAttribute<DescriptionAttribute>(prop)?.ArgumentList?.Arguments.FirstOrDefault()?.Expression;
        var description = expression !=null ? semanticModel.GetConstantValue(expression).Value as string: null;
        return string.IsNullOrWhiteSpace(description)
            ? null
            : description;
    }
        
    public static string? GetObsoleteMessage(this MemberDeclarationSyntax prop,GeneratorExecutionContext context)
    {
        var semanticModel = context.Compilation.GetSemanticModel(prop.SyntaxTree);
        var expression = GetAttribute<ObsoleteAttribute>(prop)?.ArgumentList?.Arguments.FirstOrDefault()?.Expression;
        var description = expression !=null ? semanticModel.GetConstantValue(expression).Value as string: null;
        return string.IsNullOrWhiteSpace(description)
            ? null
            : description;
    }
        
    public static AttributeSyntax? GetAttribute<T>(this MemberDeclarationSyntax prop) where T: Attribute
    {
        var attributeName = typeof(T).Name;
        return prop.AttributeLists
            .SelectMany(i => i.Attributes)
            .FirstOrDefault(i =>
                i.Name.ToString() == attributeName || i.Name.ToString() == attributeName.Replace("Attribute", ""));
            
    }

    public static IEnumerable<UsingDirectiveSyntax> GetUsingDirectives(this BaseTypeDeclarationSyntax self)
        => self.GetParentOfType<CompilationUnitSyntax>()?.Usings ?? Enumerable.Empty<UsingDirectiveSyntax>();

    public static bool HasSetter(this PropertyDeclarationSyntax self)
    {
        var setAccessor = self.AccessorList?.Accessors
            .FirstOrDefault(o => o.IsKind(SyntaxKind.SetAccessorDeclaration));
        // var hasPrivateSetter = setAccessor?.Modifiers.OfType<SyntaxToken>()
        //     .Any(i => i.IsKind(SyntaxKind.PrivateKeyword));
        return setAccessor != null;
    }
        
    public static bool HasPublicGetter(this PropertyDeclarationSyntax self)
    {
        var getAccessor = self.AccessorList?.Accessors
            .FirstOrDefault(o => o.IsKind(SyntaxKind.GetAccessorDeclaration));
        return getAccessor != null;
    }
    
    public static bool IsPublic(this PropertyDeclarationSyntax self)
    {
        return self.Modifiers.Any(i => i.IsKind(SyntaxKind.PublicKeyword));
    }

    private static bool HasAttribute<T>(this SyntaxList<AttributeListSyntax> attributeLists) where T : Attribute
    {
        return attributeLists
            .SelectMany(attributeSyntax => attributeSyntax.Attributes)
            .Any(i => i.Name.ToString() == typeof(T).Name.Replace("Attribute",""));
    }

    public static Dictionary<string?, object?> GetAttributeValues<T>(
        this MemberDeclarationSyntax prop, GeneratorExecutionContext context)
        where T: Attribute
    {
        var semanticModel = context.Compilation.GetSemanticModel(prop.SyntaxTree);
        var attributeArgs = prop.GetAttribute<T>()!.ArgumentList?.Arguments
            .ToDictionary(
                i => i.NameEquals?.Name.Identifier.ToString(),
                i => semanticModel.GetConstantValue(i.Expression).Value);
        return attributeArgs ?? [];
    }

    public static void AddFormattedSource(this GeneratorExecutionContext context, string name, string rawSource)
    {
        var tree = CSharpSyntaxTree.ParseText(rawSource);
        var root = tree.GetRoot().NormalizeWhitespace();
        var ret = root.ToFullString();
        var syntaxTree = CSharpSyntaxTree.ParseText(ret);
        var source = SourceText.From(syntaxTree.ToString(),Encoding.UTF8);
        context.AddSource($"{name}", source);
    }
}