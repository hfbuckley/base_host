using BaseHost.Generators.Helpers;
using Microsoft.CodeAnalysis;

namespace BaseHost.Generators.AspHost;

[Generator]
#pragma warning disable RS1042
public class Generator : ISourceGenerator
#pragma warning restore RS1042
{
    private static readonly string BaseHostVersion = typeof(Generator).Assembly.GetName().Version.ToString();

    public void Initialize(GeneratorInitializationContext context) { }

    public void Execute(GeneratorExecutionContext context)
    {
        var aspHost = context.GetClassesWithAttribute<AspHostAttribute>().FirstOrDefault();
        
        if(aspHost == null) return;
        
        var source = /*lang=c#*/
            $$""" 
            using System.CodeDom.Compiler;
#nullable enable

namespace {{aspHost.GetNamespace()}};

[GeneratedCode("BaseHost","{{BaseHostVersion}}")]
public partial class {{aspHost.Identifier}}: BaseHost.Web.AspNetHost {
    
    [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
    public static void Main(string[] args)
    {
        BaseHost.Web.AspNetHost.Startup<{{aspHost.Identifier}}>(args);
    }
}
""";
        context.AddFormattedSource($"{aspHost.Identifier}.generated.cs", source);
    }
}