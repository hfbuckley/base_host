namespace BaseHost.Generators.Builder;

public static class Constants
{
    public const string BuilderName = "Builder";
    public const string CreateBuilder = "CreateBuilder";
    public const string AsBuilder = "AsBuilder";

    private const string ReplacePropertyPrefix = "With";
    private const string UpdatePropertyPrefix = "Update";
    private const string AddItemToPropertyPrefix = "AddTo";
    public const string ToImmutable = "AsImmutable";
        
    public static string GetReplaceMethodName(string prop) => $"{ReplacePropertyPrefix}{prop}";
    public static string GetUpdateMethodName(string prop) => $"{UpdatePropertyPrefix}{prop}";
    public static string GetAddToMethodName(string prop) => $"{AddItemToPropertyPrefix}{prop}";
    public static string GetUpdateValueMethodName(string prop) => $"Update{prop}AtIndex";

}