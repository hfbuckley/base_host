namespace BaseHost.Generators.Builder;

public class TypeMapping(Func<string, string> toMutable, Func<string, string> toImmutable)
{
    public Func<string, string> ToMutable = toMutable;
    public Func<string, string> ToImmutable = toImmutable;
}