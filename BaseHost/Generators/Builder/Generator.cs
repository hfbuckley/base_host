﻿using System.Text;
using BaseHost.Extensions;
using BaseHost.Generators.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static BaseHost.Generators.Builder.Constants;

namespace BaseHost.Generators.Builder;

[Generator]
#pragma warning disable RS1042
public class Generator : ISourceGenerator
#pragma warning restore RS1042
{
    private ISet<string> _builderClasses = null!;
    private string _builderAccessLevel = "public";
    private static readonly string BaseHostVersion = typeof(Generator).Assembly.GetName().Version.ToString();
    
    public void Initialize(GeneratorInitializationContext context) { }

    public void Execute(GeneratorExecutionContext context)
    {
        
        _builderClasses = context.GetClassesWithAttribute<BuilderPatternAttribute>()
            .Select(i => i.Identifier.ToString())
            .Concat(context.GetRecordsWithAttribute<BuilderPatternAttribute>()
                .Select(i => i.Identifier.ToString())).ToSet();
        
        foreach (var classDeclaration in context.GetClassesWithAttribute<BuilderPatternAttribute>())
        {
            var attributeArgs = classDeclaration.GetAttributeValues<BuilderPatternAttribute>(context);
            var makePublic = attributeArgs.GetOrDefault(nameof(BuilderPatternAttribute.MakeBuilderPublic));
            _builderAccessLevel = makePublic as bool? == true ? "public" : "internal";
            var sourceBuilder = CreateSourceFileWithImports(classDeclaration);
            var className = classDeclaration.Identifier.ToString();
            sourceBuilder.Append(GenerateBuilder(classDeclaration,null, context,className));
            context.AddFormattedSource($"{className}.{BuilderName}.generated", sourceBuilder.ToString());
        }
        
        foreach (var recordDeclaration in context.GetRecordsWithAttribute<BuilderPatternAttribute>())
        {
            var attributeArgs = recordDeclaration.GetAttributeValues<BuilderPatternAttribute>(context);
            var makePublic = attributeArgs.GetOrDefault(nameof(BuilderPatternAttribute.MakeBuilderPublic));
            _builderAccessLevel = makePublic as bool? == true ? "public" : "internal";
            var sourceBuilder = CreateSourceFileWithImports(recordDeclaration);
            var className = recordDeclaration.Identifier.ToString();
            sourceBuilder.Append(GenerateBuilder(null,recordDeclaration, context,className));
            context.AddFormattedSource($"{className}.{BuilderName}.generated", sourceBuilder.ToString());
        }
    }

    private static StringBuilder CreateSourceFileWithImports(BaseTypeDeclarationSyntax typeDeclaration)
     => typeDeclaration.GetUsingDirectives()
         .Select(usingSyntax => usingSyntax.ToString())
         .Concat("using BaseHost.Extensions;") // for validation extension used inside Build()
         .Concat("using System;") // for Func<>
         .Concat("using System.Linq;")  
         .Concat("using System.Collections;")
         .Concat("using System.CodeDom.Compiler;")
         .Concat("using System.Collections.Immutable;")
         .Concat("using System.Collections.Generic;")
         .Distinct()
         .Aggregate(new StringBuilder(), (builder, s) => builder.Append($"{s}\n"));
    
    private string GenerateBuilder(
        ClassDeclarationSyntax? classSyntax, 
        RecordDeclarationSyntax? recordSyntax, 
        GeneratorExecutionContext context,
        string className)
    {
        var properties = (classSyntax?.Members ?? recordSyntax?.Members)!
            .OfType<PropertyDeclarationSyntax>()
            .Where(i => i.HasSetter())
            .Where(i=> !i.AttributeLists.SelectMany(s=> s.Attributes).Any(s=> s.Name.ToString() == "BuilderIgnoreAttribute" || s.Name.ToString() == "BuilderIgnore"))
            .ToList();

        var customValidation = (classSyntax?.GetMethodsWithAttribute<BuilderValidationAttribute>()
            ?? recordSyntax?.GetMethodsWithAttribute<BuilderValidationAttribute>())!
            .Where(i => i.TypeParameterList == null)
            .Select(i => $"\nlocal.{i.Identifier}();")
            .JoinString(string.Empty);
        
        var onBuildMethods = (classSyntax?.GetMethodsWithAttribute<OnBuildAttribute>()
            ?? recordSyntax?.GetMethodsWithAttribute<OnBuildAttribute>())!
            .Where(i => i.TypeParameterList == null)
            .Select(i => $"\nlocal.{i.Identifier}();")
            .JoinString(string.Empty);

        var classOrRecord = classSyntax == null ? "record" : "class";
        var @namespace = classSyntax?.GetNamespace() ?? recordSyntax?.GetNamespace();
            
        return /*lang=c#*/ $$"""

                 #pragma warning disable CS8604
                 #pragma warning disable CS8618
                 #pragma warning disable CS8625
                 #nullable enable

                 namespace {{@namespace}}
                 {
                     public partial {{classOrRecord}} {{className}}
                     {
                 #pragma warning disable CS8618
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         private {{className}} () { }
                         
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         {{_builderAccessLevel}} static {{className}}{{BuilderName}} {{CreateBuilder}}() => {{className}}{{BuilderName}}.Instance();
                 
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         {{_builderAccessLevel}} sealed partial class {{className}}{{BuilderName}} {
                             private {{className}} _instance = new {{className}}();
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             private {{className}}{{BuilderName}}() { }
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")] internal static {{className}}{{BuilderName}} Instance () => new {{className}}{{BuilderName}}();
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")] internal static {{className}}{{BuilderName}} BasedOn({{className}} reference) => Instance ()
                             {{CreateBasedOnSetters(properties)}};                    
                             
                             {{properties.Select(i=> CreatePropertySetter(i, context, className)).JoinString(string.Empty)}}
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             {{_builderAccessLevel}} {{className}} Build() {
                                 var local = _instance;
                                 _instance = null!;
                                 {{customValidation}}
                                 {{onBuildMethods}}
                                 local = local.GetValidatedInstance();
                                 return local;
                             }
                         }

                 #pragma warning disable CS8618
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         {{_builderAccessLevel}} sealed partial class {{className}}Mutable {
                             {{CreatePublicSetters(properties)}}
                 
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             {{_builderAccessLevel}} {{className}}Mutable(){}
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             {{_builderAccessLevel}} {{className}}Mutable({{className}} self){
                                 {{CallMutablePublicSetters(properties)}}
                             }
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             {{_builderAccessLevel}} {{className}} {{ToImmutable}}() => {{className}}.{{CreateBuilder}}()
                                 {{CreateToImmutableSetters(properties)}}
                                 .Build();
                 
                             [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                             {{_builderAccessLevel}} {{className}}Builder {{AsBuilder}}() => {{className}}.{{CreateBuilder}}()
                                 {{CreateToImmutableSetters(properties)}};                   
                         }
                     }
                 
                     [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                     {{_builderAccessLevel}} static class {{className}}Extensions{
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         {{_builderAccessLevel}} static {{className}}.{{className}}{{BuilderName}} {{AsBuilder}}(this {{className}} self) => {{className}}.{{className}}{{BuilderName}}.BasedOn(self);
                         [GeneratedCode("BaseHost","{{BaseHostVersion}}")]
                         {{_builderAccessLevel}} static {{className}}.{{className}}Mutable AsMutable(this {{className}} self) => new {{className}}.{{className}}Mutable(self);
                         {{CreateBuilderCallingMethods(properties, className, context)}}
                     }
                 }
                 """;
    }

    private string CallMutablePublicSetters(IEnumerable<PropertyDeclarationSyntax> properties)
        => properties
            .Select(p =>
            {
                var type = ImmutableHelpers.ClassifyProperty(p, out var valueType, out _);
                var hasBuilder = _builderClasses.Contains(valueType);
                var value = type switch
                {
                    ImmutableHelpers.ImmutableTypes.Dictionary or ImmutableHelpers.ImmutableTypes.OtherDictionary =>
                        hasBuilder
                            ? $"self.{p.Identifier}.Select(i=> (value:i.Value.AsMutable(), key:i.Key)).ToDictionary(i=> i.key,i=> i.value)"
                            : $"self.{p.Identifier}.ToDictionary(i=> i.Key,i=> i.Value)",
                    ImmutableHelpers.ImmutableTypes.OtherArray or ImmutableHelpers.ImmutableTypes.Array => hasBuilder
                        ? $"self.{p.Identifier}.Select(i=> i.AsMutable()).ToList()"
                        : $"self.{p.Identifier}.ToList()",
                    ImmutableHelpers.ImmutableTypes.HashSet or ImmutableHelpers.ImmutableTypes.OtherSet => hasBuilder
                        ? $"self.{p.Identifier}.Select(i=> i.AsMutable()).ToSet()"
                        : $"self.{p.Identifier}.ToSet()",
                    _ => hasBuilder ? $"self.{p.Identifier}.AsMutable()" : $"self.{p.Identifier}"
                };
                return $"{p.Identifier} = {value};";
            })
            .JoinString(Environment.NewLine);
        
    private string CreatePublicSetters(IEnumerable<PropertyDeclarationSyntax> properties)
        => properties
            .Select(p =>
            {
                var type = p.Type.ToString();
                var immutableType = ImmutableHelpers.ClassifyProperty(p, out var valueType, out var indexType);
                var indexType2 = _builderClasses.Contains(indexType) ? $"{indexType}.{indexType}Mutable" : indexType;
                var valueType2 = _builderClasses.Contains(valueType) ? $"{valueType}.{valueType}Mutable" : valueType;
                    
                type = immutableType switch
                {
                    ImmutableHelpers.ImmutableTypes.Array => $"List<{valueType2}>",
                    ImmutableHelpers.ImmutableTypes.OtherArray => $"List<{valueType2}>",
                    ImmutableHelpers.ImmutableTypes.Dictionary => $"Dictionary<{indexType2},{valueType2}>",
                    ImmutableHelpers.ImmutableTypes.OtherDictionary => $"Dictionary<{indexType2},{valueType2}>",
                    ImmutableHelpers.ImmutableTypes.HashSet => $"ISet<{valueType2}>",
                    ImmutableHelpers.ImmutableTypes.OtherSet => $"ISet<{valueType2}>",
                    _ => type
                };
                var mutable = _builderClasses.Contains(type) ? $".{type}Mutable" : string.Empty;
                return $"[GeneratedCode(\"BaseHost\",\"{BaseHostVersion}\")] {_builderAccessLevel} {type}{mutable} {p.Identifier.ToString()} {{get; set; }}";
            })
            .JoinString(Environment.NewLine);

    private string CreateBuilderCallingMethods(
        IEnumerable<PropertyDeclarationSyntax> properties,
        string className,
        GeneratorExecutionContext context)
        => properties.Select(syntax => CreateBuilderCallers(syntax, context,className)).JoinString(string.Empty);
        
    private static string CreateBasedOnSetters(IEnumerable<PropertyDeclarationSyntax> propNames)
        => propNames.Select(p=> p.Identifier.ToString())
            .Select(name => $".{GetReplaceMethodName(name)}(reference.{name})")
            .JoinString(string.Empty);
        
    private string CreateToImmutableSetters(
        IEnumerable<PropertyDeclarationSyntax> propNames)
        => propNames
            .Select(p =>
            {
                var name = p.Identifier.ToString();

                var type = ImmutableHelpers.ClassifyProperty(p, out var valueType, out _);
                var hasBuilder = _builderClasses.Contains(valueType);
                var value = type switch
                {
                    ImmutableHelpers.ImmutableTypes.Dictionary or ImmutableHelpers.ImmutableTypes.OtherDictionary =>
                        hasBuilder
                            ? $"this.{p.Identifier}.Select(i=> (value:i.Value.{ToImmutable}(), key:i.Key)).ToDictionary(i=> i.key,i=> i.value).ToImmutableDictionary()"
                            : $"this.{p.Identifier}.ToDictionary(i=> i.Key,i=> i.Value).ToImmutableDictionary()",
                    ImmutableHelpers.ImmutableTypes.OtherArray or ImmutableHelpers.ImmutableTypes.Array => hasBuilder
                        ? $"this.{p.Identifier}.Select(i=> i.{ToImmutable}()).ToList().ToImmutableArray()"
                        : $"this.{p.Identifier}.ToList().ToImmutableArray()",
                    ImmutableHelpers.ImmutableTypes.HashSet or ImmutableHelpers.ImmutableTypes.OtherSet => hasBuilder
                        ? $"this.{p.Identifier}.Select(i=> i.{ToImmutable}()).ToSet().ToImmutableHashSet()"
                        : $"this.{p.Identifier}.ToSet().ToImmutableHashSet()",
                    _ => hasBuilder
                        ? $"this.{p.Identifier}.{ToImmutable}()"
                        : $"this.{p.Identifier}"
                };

                return $".{GetReplaceMethodName(name)}({value})";
            })
            .JoinString(string.Empty);
        
    private string CreateBuilderCallers(
        PropertyDeclarationSyntax property,
        GeneratorExecutionContext context,
        string className)
    {
        var propertyName = property.Identifier.ToString();
        var localPropertyName = propertyName.ToCamelCase();
        var typeName = property.Type.ToString();
        return /*lang=c#*/ $"""
                                       {ImmutableHelpers.BuilderSetter(property,context,className)}
                                       [GeneratedCode("BaseHost", "{BaseHostVersion}")] {_builderAccessLevel} static {className} {GetReplaceMethodName(propertyName)}(this {className} self,{typeName} {localPropertyName}) => self.AsBuilder().{GetReplaceMethodName(propertyName)}({localPropertyName}).Build();
                                       [GeneratedCode("BaseHost", "{BaseHostVersion}")] {_builderAccessLevel} static {className} {GetUpdateMethodName(propertyName)}(this {className} self,Func<{typeName},{typeName}> {localPropertyName}Func) => self.AsBuilder().{GetUpdateMethodName(propertyName)}({localPropertyName}Func).Build();
                           """; 
    }

        
    private string CreatePropertySetter(PropertyDeclarationSyntax property, GeneratorExecutionContext context,string className)
    {
        var propertyName = property.Identifier.ToString();
        var localPropertyName = propertyName.ToCamelCase();
        var typeName = property.Type.ToString();
        return /*lang=c#*/ $$"""
                            
                                            {{ImmutableHelpers.PropertySetter(property, context,className)}}
                                            [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                                            {{_builderAccessLevel}} {{className}}{{BuilderName}} {{GetReplaceMethodName(propertyName)}}({{typeName}} {{localPropertyName}}){
                                                _instance.{{propertyName}} = {{localPropertyName}};
                                                return this;
                                            }
                                            [GeneratedCode("BaseHost", "{{BaseHostVersion}}")]
                                            {{_builderAccessLevel}} {{className}}{{BuilderName}} {{GetUpdateMethodName(propertyName)}}(Func<{{typeName}},{{typeName}}> {{localPropertyName}}Func){
                                                _instance.{{propertyName}} = {{localPropertyName}}Func(_instance.{{propertyName}});
                                                return this;
                                            }
                            """; 
    }
}