using System.Collections;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using BaseHost.Generators.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using static BaseHost.Generators.Builder.Constants;

namespace BaseHost.Generators.Builder;

public class ImmutableHelpers
{
    public enum ImmutableTypes
    {
        Other,
        Array,
        Dictionary,
        HashSet,
        OtherArray,
        OtherDictionary,
        OtherSet
    }
        
    public static ImmutableTypes ClassifyProperty(
        PropertyDeclarationSyntax property,
        out string valueType,
        out string indexType)
    {
        var typeName = property.Type.ToString();
        var type = ImmutableTypes.Other;
        valueType = property.Type.ToString();
        indexType = null!;
        if (property.Type is not GenericNameSyntax genericType) return ImmutableTypes.Other;
        if (typeName.StartsWith(nameof(ImmutableHashSet))) type = ImmutableTypes.HashSet;
            
        if (typeName.StartsWith(nameof(ImmutableArray))) type= ImmutableTypes.Array;

        if (typeName.StartsWith(nameof(ImmutableDictionary))) type= ImmutableTypes.Dictionary;
            
        if (typeName.StartsWith("Dictionary")) type= ImmutableTypes.OtherDictionary;
        if (typeName.StartsWith("IDictionary")) type= ImmutableTypes.OtherDictionary;
        if (typeName.StartsWith("ISet")) type = ImmutableTypes.OtherSet;
        if (typeName.StartsWith("HashSet")) type = ImmutableTypes.OtherSet;
        if (typeName.StartsWith(nameof(IEnumerable))) type= ImmutableTypes.OtherArray;
        if (typeName.StartsWith(nameof(Enumerable))) type= ImmutableTypes.OtherArray;
        if (typeName.StartsWith("List")) type= ImmutableTypes.OtherArray;

        valueType = genericType.TypeArgumentList.Arguments.Last().ToString();
        indexType = genericType.TypeArgumentList.Arguments.Count == 1 
            ? "int" 
            : genericType.TypeArgumentList.Arguments.First().ToString();
        return type;
    }
        
    public static string PropertySetter(PropertyDeclarationSyntax property, GeneratorExecutionContext context,string className)
    {
        var propertyName = property.Identifier.ToString();
        var typeName = property.Type.ToString();

        var type = ClassifyProperty(property, out var valueType, out var indexType);
        if (type != ImmutableTypes.Dictionary && type!= ImmutableTypes.Array) return string.Empty;

        var genericTypeString = string.Concat(typeName.TakeWhile(i => i != '<'));
        var genericTypes = type != ImmutableTypes.Dictionary
            ? valueType
            : $"{indexType},{valueType}";

        var getBuilder =
            $"_instance.{propertyName} == default? " +
            $"{genericTypeString}.CreateBuilder<{genericTypes}>(): _instance.{propertyName}.ToBuilder();";
                 
        var immutableMethods = $$"""
                                 
                                                     [GeneratedCode("BaseHost", null)]
                                                     public {{className}}{{BuilderName}} {{GetAddToMethodName(propertyName)}}({{indexType}} index, {{valueType}} value){
                                                         var builder = {{getBuilder}}
                                                         builder[index] = value;    
                                                         _instance.{{propertyName}} = builder.ToImmutable();
                                                         return this;
                                                     }
                                                     [GeneratedCode("BaseHost", null)]
                                                     public {{className}}{{BuilderName}} {{GetUpdateValueMethodName(propertyName)}}({{indexType}} index, Func<{{valueType}},{{valueType}}> valueFunc){
                                                         var builder = {{getBuilder}}
                                                         builder[index] = valueFunc(builder[index]);    
                                                         _instance.{{propertyName}} = builder.ToImmutable();
                                                         return this;
                                                     }
                                 """;

        var keyValue = GetKeyAttribute(context, valueType, indexType);
        return type switch
        {
            ImmutableTypes.Dictionary when keyValue != null => $$"""
                                                                 {{immutableMethods}}
                                                                                         [GeneratedCode("BaseHost", null)]
                                                                                         public {{className}}{{BuilderName}} {{GetAddToMethodName(propertyName)}}({{valueType}} value){
                                                                                             var builder = {{getBuilder}}
                                                                                             builder[value.{{keyValue.Identifier.ToString()}}] = value;    
                                                                                             _instance.{{propertyName}} = builder.ToImmutable();
                                                                                             return this;
                                                                                         }
                                                                 """,
            ImmutableTypes.Array => $$"""
                                      
                                                           {{immutableMethods}}
                                                           [GeneratedCode("BaseHost", null)]
                                                           public {{className}}{{BuilderName}} {{GetAddToMethodName(propertyName)}}({{valueType}} value){
                                                              var builder = {{getBuilder}}
                                                              builder.Add(value);    
                                                              _instance.{{propertyName}} = builder.ToImmutable();
                                                              return this;
                                                           }
                                      """,
            _ => immutableMethods
        };
    }


    public static string BuilderSetter(PropertyDeclarationSyntax property,
        GeneratorExecutionContext context,
        string className)
    {
        var propertyName = property.Identifier.ToString();
        var type = ClassifyProperty(property, out var valueType, out var indexType);
            
        if (type != ImmutableTypes.Dictionary && type!= ImmutableTypes.Array) return string.Empty;
            
        var immutableArrayMethods = 
            $"""
             
                             [GeneratedCode("BaseHost", null)] public static {className} {GetAddToMethodName(propertyName)}(this {className} self,{indexType} index, {valueType} value) =>  self.AsBuilder().{GetAddToMethodName(propertyName)}(index,value).Build();
                             [GeneratedCode("BaseHost", null)] public static {className} {GetUpdateValueMethodName(propertyName)}(this {className} self,{indexType} index, Func<{valueType},{valueType}> valueFunc) => self.AsBuilder().{GetUpdateValueMethodName(propertyName)}(index,valueFunc).Build();
             """;
                
        var keyValue = GetKeyAttribute(context, valueType, indexType);
        if (keyValue != null)
        {
            immutableArrayMethods = 
                $"""
                 {immutableArrayMethods}
                                 [GeneratedCode("BaseHost", null)] public static {className} {GetAddToMethodName(propertyName)}(this {className} self,{valueType} value) => self.AsBuilder().{GetAddToMethodName(propertyName)}(value.{keyValue.Identifier.ToString()},value).Build(); 
                 """;
        }
        return immutableArrayMethods;
    }
        
    private static PropertyDeclarationSyntax? GetKeyAttribute(GeneratorExecutionContext context, string valueType, string indexType)
        => context.GetClasses()
            .Where(i => i.Identifier.ToString() == valueType)
            .SelectMany(i=> i.GetPropertiesWithAttribute<KeyAttribute>())
            .FirstOrDefault(i => i.Type.ToString() == indexType);
}