using BaseHost.Host;

namespace BaseHost.Generators;

//not ready yet
[AttributeUsage(AttributeTargets.Class)]
public class AspHostAttribute<T> : Attribute where T : Options;

[AttributeUsage(AttributeTargets.Class)]
public class AspHostAttribute : Attribute;

[AttributeUsage(AttributeTargets.Property | AttributeTargets.Parameter, AllowMultiple = true)]
public class ExampleAttribute : Attribute
{
    public string? Name { get; set; }
    public string? Description { get; set; }
    public string? Value { get; set; }

    public Types Type { get; set; } = Types.String;

    public enum Types
    {
        String,
        Int,
        Bool,
        Byte,
        Double,
        DateTime,
        Date,
        Object
    }
}