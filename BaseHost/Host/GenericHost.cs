﻿using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reflection;
using Autofac;
using BaseHost.Modules.Config;
using BaseHost.Modules.Logging;
using CommandLine;
using Microsoft.Extensions.Configuration;
using NLog;

namespace BaseHost.Host;

public abstract class GenericHost : IHostDisposable
{
    private readonly ManualResetEvent _resetEvent = new(false);
    
    protected abstract void Setup(ContainerBuilder builder);

    internal virtual void InternalSetup(ContainerBuilder builder)
    {
        builder.RegisterModule<NLogModule>();
        builder.RegisterModule<ConfigModule>();
    }
    
    protected void Start<T>(string[] args) where T: Options
    {
        Parser.Default.ParseArguments<T>(args)
            .WithParsed(s =>
            {
                var env = s.Environment
                          ?? Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT")
                          ?? Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")
                          ?? "Production";
                
                var config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true)
                    .Build();
                
                IContainer? container = null;
                var name = s.ApplicationName ?? Assembly.GetEntryAssembly()?.GetName().Name;
                NLogConfig.Setup($"{s.LogLocation}{name}.log", s.ConsoleLogLevel?? "error",s.LogLevel??"debug");

                
                var logger = LogManager.GetCurrentClassLogger();
                logger.Info($"Application name: {name}");
                logger.Info($"Application version: {Assembly.GetEntryAssembly()?.GetName().Version}");
                logger.Info($"Started by User: {Environment.UserName}");
                logger.Info($"Cmd: {Environment.CommandLine}");
                logger.Info($"Env: {env}");
                logger.Info($"Location: {Environment.CurrentDirectory}");
                logger.Info($"Log:{s.LogLocation}{name}.log");
                
                Observable.Return(Unit.Default)
                    .SubscribeOn(TaskPoolScheduler.Default)
                    .Take(1)
                    .Subscribe(_ =>
                    {
                        try
                        {
                            var containerBuilder = new ContainerBuilder();
                            containerBuilder
                                .RegisterInstance(this)
                                .As<IHostDisposable>();
                            
                            containerBuilder.RegisterInstance<IConfiguration>(config);
                            containerBuilder.RegisterInstance(s)
                                .AsSelf().
                                AsImplementedInterfaces();
                            
                            InternalSetup(containerBuilder);
                            Setup(containerBuilder);
                            container = containerBuilder.Build();
                        }
                        catch (Exception ex)
                        {
                            LogManager
                                .GetLogger("ExceptionLogger")
                                .Error(ex, "An exception has been thrown, will now close.");
                            _resetEvent.Set();
                        }
                    });
                _resetEvent.WaitOne();
                logger.Info("Host Disposing");
                container?.Dispose();
            })
            .WithNotParsed(_ =>
            {

            });
    }

    public void Dispose() => _resetEvent.Set();
}