﻿using System.Reactive.Concurrency;

namespace BaseHost.Host;

public interface ISchedulerProvider
{
    IScheduler TaskPool { get; }
    IScheduler NewThread { get; }
    IScheduler CurrentThread { get; }
    IScheduler Immediate { get; }
    IScheduler Ui { get; }
}