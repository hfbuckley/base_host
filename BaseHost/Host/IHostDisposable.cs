﻿namespace BaseHost.Host;

public interface IHostDisposable : IDisposable;