using CommandLine;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace BaseHost.Host;

public record Options
{
    [Option("env", Required = false, HelpText = "Environment of the config")]
    public string? Environment { get; set; }
    
    [Option("name", Required = false, HelpText = "Name of application")]
    public string? ApplicationName { get; set; }
    
    [Option("logPath", Required = false, HelpText = "Set output to verbose messages.")]
    public string? LogLocation { get; set; }
    
    [Option('l',"log", Required = false, HelpText = "Level of log messages")]
    public string? LogLevel { get; set; }
    
    [Option('c',"console-log", Required = false, HelpText = "Level of log messages sent to console")]
    public string? ConsoleLogLevel { get; set; }
}