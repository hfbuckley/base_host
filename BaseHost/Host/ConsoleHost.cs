﻿using Autofac;
using BaseHost.Modules.Console;

namespace BaseHost.Host;

public abstract class ConsoleHost : GenericHost
{
    internal override void InternalSetup(ContainerBuilder builder)
    {
        base.InternalSetup(builder);
        builder.RegisterModule<TerminalModule>();
        builder.RegisterModule<LocalTerminalModule>();

        builder
            .RegisterType<ConsoleSchedulerProvider>()
            .As<ISchedulerProvider>()
            .SingleInstance();
    }
}