﻿using System.Reactive.Concurrency;

namespace BaseHost.Host;

public class ConsoleSchedulerProvider : ISchedulerProvider
{
    public IScheduler TaskPool { get; } = TaskPoolScheduler.Default;
    public IScheduler NewThread => NewThreadScheduler.Default;
    public IScheduler CurrentThread => Scheduler.CurrentThread;
    public IScheduler Immediate { get; } = Scheduler.Immediate;
    public IScheduler Ui { get; } = NewThreadScheduler.Default;
}