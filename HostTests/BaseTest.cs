﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using Microsoft.Reactive.Testing;

namespace HostTests;

public class BaseTest : ReactiveTest
{
    protected static readonly IFixture Fixture = CreateFixture();

    private static IFixture CreateFixture()
    {
        return new Fixture()
            .Customize(new AutoNSubstituteCustomization { ConfigureMembers = true});
    }
}