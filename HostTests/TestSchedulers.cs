using System.Reactive.Concurrency;
using BaseHost.Host;
using Microsoft.Reactive.Testing;

namespace HostTests;

public class TestSchedulers : ISchedulerProvider
{
    public IScheduler TaskPool { get; } = new TestScheduler();
    public IScheduler NewThread { get; } = new TestScheduler();
    public IScheduler CurrentThread { get; } = new TestScheduler();
    public IScheduler Immediate { get; } = new TestScheduler();
    public IScheduler Ui { get; } = new TestScheduler();
}