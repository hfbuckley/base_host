﻿using Autofac;
using BaseHost.Extensions;
using BaseHost.Generators;
using BaseHost.Host;

namespace HostRunner;

public class Program : ConsoleHost
{
    public static void Main(string[] args) => new Program().Start<Options>(args);
    
    protected override void Setup(ContainerBuilder builder)
    {
        var t = new ItemSet()
        {
            Slot1 = "1",
            Slot2 = "2",
            Slot3 = "3",
        };

        var t2 = t with
        {
            Slot1 = t.Slot2,
            Slot2 = t.Slot1,
        };

        var t3 = t2 with
        {
            Slot1 = null
        };

        var t0 = t3 with { };

        var t4 = t3.WithUpdated(0, _ => "-1");
        
        var t5 = t4.WithUpdated((_,i) => i==null? null: $"{i}.2");

        Console.Out.Write((t0, t5).ToString());
        
        builder.AutoActivateWith<Controller>(i => i.Start());
    }
}

[Collection(Size = 4, Type ="string")]
public partial record ItemSet;