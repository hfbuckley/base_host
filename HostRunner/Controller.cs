using BaseHost.Extensions;
using BaseHost.Modules.Config;
using BaseHost.Modules.Console.Cli;
using BaseHost.Modules.Console.Console;
using HostRunner.Models;
using HostRunner.OtherThings;
using HostRunner.OtherThings2;
using Microsoft.Extensions.Configuration;
using NLog;

namespace HostRunner;

[Config("Example")]
public record ExampleSettings(
    string One = "Default One", 
    string Two = "Default Two");

public class Controller(ILogger logger, ExampleSettings config, IConfiguration configuration)
{
    public void Start()
    {
        var env = configuration["Env"]!;
        logger.Info($"ENV: {env}");
        logger.Info("this is Info");
        logger.Debug("this is Debug");
        logger.Warn("Warning");
        logger.Error("Error");
        logger.Fatal("Fatal");
        logger.Trace("Trace");
        var moves = new[] { "One","Two","Three","Four" };
        var targets = new[] { "All","One" };
        var pokemon = new[] { "One","Two","Three","Four" };
        
        var menu = new Menu<string>(config.Two,$"Please Choose an action {config.Two} {config.Two}")
            .AddOption(config.One, 
                m=> moves
                    .ForEach(i=> m
                        .AddOption(i, menu=> targets.ForEach(o=> menu.AddOption(o,o)))))
            .AddOption("Pokemon", 
                m=> pokemon
                    .ForEach(i=> m.AddOption(i, i)))
            .AddOption("Run", "Tackle")
            .AddOption("Item", "Tackle");

        var controller = new MenuController<string>(new LocalConsole(), menu);
        controller.GetSelection();


        var th = new MergedProperties
        {
            InnerModel = new ModelWithLotsOfProperties1
            {
                Property1 = "2",
                EnumThing = EnumThing.Four
            },
            InnerModel2 = new ModelWithLotsOfProperties2
            {
                Property3 = "12"
            },
            InnerModel3 = new ModelWithLotsOfProperties3
            {
                Thing = EnumThing2.Four
            },
            InnerModel4 = new ModelWithLotsOfProperties4("the")
        };
        var t = new NestedMergedProperties
        {
            InnerModel1213 = th
        };
        
        Console.Out.Write(th.InnerModel.EnumThing);
        Console.Out.Write(th.InnerModel3.Thing);
        Console.Out.Write(t.ToJson());
    }
}