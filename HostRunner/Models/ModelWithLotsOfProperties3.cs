using System.ComponentModel;
using HostRunner.OtherThings2;
using JetBrains.Annotations;

namespace HostRunner.Models;

[PublicAPI]
public record ModelWithLotsOfProperties3
{
    [Description("Property4 ")] public string Property4 => null!;

    public EnumThing2 Thing { get; set; }
}