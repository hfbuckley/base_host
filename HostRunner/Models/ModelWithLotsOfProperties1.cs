using HostRunner.OtherThings;
using JetBrains.Annotations;

namespace HostRunner.Models;

[PublicAPI]
public class ModelWithLotsOfProperties1
{
    public string Property1 { get; init; } = null!;
    
    public EnumThing EnumThing { get; init; }
}