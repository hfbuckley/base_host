using BaseHost.Generators;
using JetBrains.Annotations;

namespace HostRunner.Models;

[PublicAPI]
public partial class MergedProperties
{
    public string Property2 { get; } = null!;
    
    [IncludeProperties] public ModelWithLotsOfProperties1 InnerModel { get; set; } = null!;
    
    [IncludeProperties] public ModelWithLotsOfProperties2 InnerModel2 { get; set; }= null!;
    
    [IncludeProperties] public ModelWithLotsOfProperties3 InnerModel3 { get; set; }= null!;
    
    [IncludeProperties] public ModelWithLotsOfProperties4 InnerModel4 { get; set; }= null!;
    
}