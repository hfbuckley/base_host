using System.Collections.Immutable;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BaseHost.Generators;
using JetBrains.Annotations;

namespace HostRunner.Models;

[BuilderPattern]
[PublicAPI]
public partial class ClassModel
{
    private string PrivateValue { get; set; }
    public string Value2 { get; private set; }
    [Description("This is a value ..."),Required]
    public string Value5 { get; private set; }
    public ImmutableHashSet<int> Valeue6E { get; private set; }
        
    public ImmutableArray<int> Valeuee6E { get; private set; }
        
    public ImmutableDictionary<string,ClassModel> Valeduee6E { get; private set; }
    public ImmutableDictionary<string,int> Valed2Uee6E { get; private set; }


    public string? NullableString { get; private set; }
    public string NonNullableString { get; private set; }

    public Model3 InnerModel { get; private set; }

    [BuilderIgnore]
    public string NonNullableString2 { get; private set; }
    
    
    [OnBuild]
    public void FinishedSetup()
    {
        NonNullableString2 = NonNullableString;
        //this.AsMutable().In
    } 
    
    public static void The(ClassModel t)
    {
        //  var tfr = Valeduee6e!.ToDictionary().ToImmutableDictionary();

        t.AsBuilder().WithPrivateValue("").WithNullableString(null).WithNonNullableString("null");
    }
}

public interface INothing;

[PublicAPI]
public record ModelWithLotsOfProperties2
{
    [Description("THE")]
    [Required]
    public required string Property3 { get; init; }
}

[PublicAPI]
public record ModelWithLotsOfProperties4([property: Description("Property5 ")] string Property5);

[PublicAPI]
public partial class NestedMergedProperties
{
    [IncludeProperties] public MergedProperties InnerModel1213 { get; set; } = null!;
}