using JetBrains.Annotations;

namespace HostRunner.OtherThings2;

[PublicAPI]
public enum EnumThing2
{
    One,
    Two,
    Three,
    Four
}