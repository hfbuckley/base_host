using JetBrains.Annotations;

namespace HostRunner.OtherThings;

[PublicAPI]
public enum EnumThing
{
    One,
    Two,
    Three,
    Four
}